# Quiz

Description

Quiz is a HR tool

## Getting started
```
git clone https://gitlab.com/claudiu.brandabur/quiz_hr_tool.git

npm install

ng serve

localhost:4200
```

## Setting the database

### Step 1

   Install PostgreSQL. [Download link](https://www.enterprisedb.com/downloads/postgres-postgresql-downloads#windows)

### Step 2

   Set password for postgreSQL (by default, the username is postgres). Do not forget the password for the user postgres!

### Step 3

   Set the port (by default, the port is 5432)

### Step 4

   Connect to pgAdmin4

   ![](guide/step4.png)

#### Step 4.1

   This page opens in the browser. Expand Servers.

   ![](guide/step4_1.png)

#### Step 4.2

   Right click on PostgreSQL and press Connect to Server and insert the password.

   ![](guide/step4_2.png)

#### Step 4.3

   Right click on Login/Group roles and press Create Login/Group Role.

   ![](guide/step4_3.png)

#### Step 4.4

   Insert name quiz in field Name from tab General.

   ![](guide/step4_4.png)

#### Step 4.5

   Set password quiz in field Password from tab Definition.

   ![](guide/step4_5.png)

#### Step 4.6

   Set to ‘Yes’ all the fields from tab Privileges and press Save.

   ![](guide/step4_6.png)

#### Step 4.7

   Right click on PostgreSQL and select Create Database.

   ![](guide/step4_7.png)

#### Step 4.8

   Insert quiz for field Database, select quiz as Owner in General tab and press Save.

   ![](guide/step4_8.png)

### Step 5

   This will generate the following.

   ![](guide/step5.png)

### Step 6

   Next time we start the application, in the public Schema, we will expand Tables and we will find our tables.
