export class SignUpInfo {
  firstName: string;
  lastName: string;
  username: string;
  email: string;
  phone: string;
  password: string;
  userRoles: string[];

  constructor(firstName: string,
              lastName: string,
              username: string,
              email: string,
              phone: string,
              password: string,
              userRole: string) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.username = username;
    this.email = email;
    this.phone = phone;
    this.password = password;
    // this.userRoles = [userRole];
    this.userRoles.push(userRole);
  }
}
