export class JwtResponse {
    authToken: string;
    tokenType: string;
    authUsername: string;
    authAuthorities: string[];
}
