package com.open.quiz.controller;

import com.open.quiz.service.AnswerService;
import com.open.quiz.service.dto.AnswerDTO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

//@RunWith(SpringRunner.class)
//@WebMvcTest(AnswerController.class)
public class AnswerControllerTest {

//    @MockBean
//    private AnswerService answerService;
//
//    @Autowired
//    private MockMvc mockMvc;
//
//    @Before
//    public void setUp(){
//        when(answerService.findAnswerById(1L)).thenReturn(getDTO());
//    }
//
//    @Test
//    public void testFindAllAnswers() throws Exception {
//
//        List<AnswerDTO> allAnswers = Arrays.asList(getDTO());
//        given(answerService.findAllAnswers()).willReturn(allAnswers);
//
//        mockMvc.perform(get("/api/answers")
//            .contentType(MediaType.APPLICATION_JSON))
//            .andExpect(status().isOk())
//            .andExpect(jsonPath("$", hasSize(1)));
//    }
//
//    @Test
//    public void testFindAnswerById() throws Exception{
//
//        mockMvc.perform(get("/api/answers/1"))
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
//            .andExpect(status().isOk());
//
//        verify(answerService, times(1)).findAnswerById(1L);
//    }
//
//
//    private AnswerDTO getDTO(){
//        AnswerDTO answer = new AnswerDTO();
//        answer.setId(1l);
//        answer.setAnswerText("ANSWER_TEXT");
//        answer.setQuestionId(1l);
//
//        return answer;
//    }

}
