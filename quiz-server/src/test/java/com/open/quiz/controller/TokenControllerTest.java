package com.open.quiz.controller;

import com.open.quiz.service.TokenService;
import com.open.quiz.service.dto.TokenDTO;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Ignore
@RunWith(SpringRunner.class)
@WebMvcTest(TokenController.class)
public class TokenControllerTest {

    @MockBean
    private TokenService tokenService;

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void setUp() {
        when(tokenService.findTokenById(1L)).thenReturn(getDTO());
//        tokenService.createToken(getDTO());
    }

    @Test
    public void testCreateToken() throws Exception {
        TokenDTO tokenDTO = getDTO();
        given(tokenService.createToken(tokenDTO)).willReturn(tokenDTO);

        mockMvc.perform(get("/api/tokens/1"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        verify(tokenService, times(1)).findTokenById(1L);
    }

    @Test
    public void testGetTokenById() throws Exception {

        mockMvc.perform(get("/api/tokens/1"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        verify(tokenService, times(1)).findTokenById(1L);
    }

    @Test
    public void testGetAllTokens() throws Exception {

        List<TokenDTO> allTokens = Arrays.asList(getDTO());
        given(tokenService.findAllTokens()).willReturn(allTokens);

        mockMvc.perform(get("/api/tokens")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)));
    }

    @Test
    public void testUpdateToken() throws Exception {
    }

    @Test
    public void testDeleteTokenById() throws Exception {
    }

    @Test
    public void testDeleteToken() throws Exception {
    }

    @Test
    public void testDeleteAllTokens() throws Exception {
    }

    private TokenDTO getDTO() {
        TokenDTO tokenDTO = new TokenDTO();
        tokenDTO.setId(1L);
        tokenDTO.setToken("TOKEN_TEXT_TEST");
        tokenDTO.setTimeOfSolving(30);
        tokenDTO.setValability(LocalDateTime.of(2010, 1, 1, 1, 0));
        tokenDTO.setParticipantId(1L);
        tokenDTO.setQuizId(1L);
        return tokenDTO;
    }

}
