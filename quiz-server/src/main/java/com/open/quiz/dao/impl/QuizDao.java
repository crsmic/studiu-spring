package com.open.quiz.dao.impl;


import com.open.quiz.dao.BaseDao;
import com.open.quiz.model.Quiz;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class QuizDao extends BaseDao {

    public Quiz createQuiz(Quiz quiz) {
        em().persist(quiz);
        return quiz;
    }

    public Quiz updateQuiz(Quiz quiz) {
        return em().merge(quiz);
    }

    public Quiz findQuizById(Long id) {
        return em().find(Quiz.class, id);
    }

    public void deleteQuiz(Quiz quiz) {
        em().remove(quiz);
    }

    public void deleteQuizById(Long id) {
        Quiz quiz = findQuizById(id);
        deleteQuiz(quiz);
    }

    public List<Quiz> findAllQuizs() {
        return em().createQuery("SELECT t FROM " + Quiz.class.getSimpleName() + " t").getResultList();
    }

    public void deleteAllQuizs() {
        List<Quiz> quizs = findAllQuizs();
        for (Quiz quiz : quizs) {
            deleteQuiz(quiz);
        }
    }
}
