package com.open.quiz.dao.impl;

import com.open.quiz.dao.BaseDao;
import com.open.quiz.model.Privilege;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PrivilegeDAO extends BaseDao {

    public Privilege createPrivilege(Privilege privilege) {
        em().persist(privilege);
        return privilege;
    }

    public Privilege findPrivilegeById(Long id) {
        return em().find(Privilege.class, id);
    }

    public Privilege findPrivilegeByName(String name) {
        return em().createQuery("SELECT c FROM " + Privilege.class.getSimpleName() + " c WHERE c.name = :name", Privilege.class)
                .setParameter("name", name).getSingleResult();
    }

    public List<Privilege> findAllPrivileges() {
        return em().createQuery("SELECT t FROM " + Privilege.class.getSimpleName() + " t").getResultList();
    }

    public Privilege updatePrivilege(Privilege privilege) {
        return em().merge(privilege);
    }

    public void deletePrivilegeById(Long id) {
        em().remove(findPrivilegeById(id));
    }

    public void deletePrivilege(Privilege privilege) {
        em().remove(privilege);
    }

    public void deleteAllPrivileges() {
        List<Privilege> privileges = findAllPrivileges();
        for (Privilege privilege : privileges) {
            deletePrivilege(privilege);
        }
    }
}
