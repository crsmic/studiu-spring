package com.open.quiz.dao.impl;

import com.open.quiz.dao.BaseDao;
import com.open.quiz.model.QuizParticipant;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class QuizParticipantDao extends BaseDao {

    public QuizParticipant createQuizParticipant(QuizParticipant quizParticipant) {
        em().persist(quizParticipant);
        return quizParticipant;
    }

    public QuizParticipant updateQuizParticipant(QuizParticipant quizParticipant) {
        return em().merge(quizParticipant);
    }

    public QuizParticipant findQuizParticipantById(Long id) {
        return em().find(QuizParticipant.class, id);
    }

    public void deleteQuizParticipant(QuizParticipant quizParticipant) {
        em().remove(quizParticipant);
    }

    public void deleteQuizParticipantById(Long id) {
        QuizParticipant quizParticipant = findQuizParticipantById(id);
        deleteQuizParticipant(quizParticipant);
    }

    public List<QuizParticipant> findAllQuizParticipants() {
        return em().createQuery("SELECT t FROM " + QuizParticipant.class.getSimpleName() + " t").getResultList();
    }

    public void deleteAllQuizParticipants() {
        List<QuizParticipant> quizParticipants = findAllQuizParticipants();
        for (QuizParticipant quizParticipant : quizParticipants) {
            deleteQuizParticipant(quizParticipant);
        }
    }
}
