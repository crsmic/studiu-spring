package com.open.quiz.dao.impl;

import com.open.quiz.dao.BaseDao;
import com.open.quiz.model.QuizParticipantAnswer;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class QuizParticipantAnswerDao extends BaseDao {

    public QuizParticipantAnswer createQuizParticipantAnswer(QuizParticipantAnswer quizParticipantAnswer) {
        em().persist(quizParticipantAnswer);
        return quizParticipantAnswer;
    }

    public QuizParticipantAnswer updateQuizParticipantAnswer(QuizParticipantAnswer quizParticipantAnswer) {
        return em().merge(quizParticipantAnswer);
    }

    public QuizParticipantAnswer findQuizParticipantAnswerById(Long id) {
        return em().find(QuizParticipantAnswer.class, id);
    }

    public void deleteQuizParticipantAnswer(QuizParticipantAnswer quizParticipantAnswer) {
        em().remove(quizParticipantAnswer);
    }

    public void deleteQuizParticipantAnswerById(Long id) {
        QuizParticipantAnswer quizParticipantAnswer = findQuizParticipantAnswerById(id);
        deleteQuizParticipantAnswer(quizParticipantAnswer);
    }

    public List<QuizParticipantAnswer> findAllQuizParticipantAnswers() {
        return em().createQuery("SELECT t FROM " + QuizParticipantAnswer.class.getSimpleName() + " t").getResultList();
    }

    public void deleteAllQuizParticipantAnswers() {
        List<QuizParticipantAnswer> quizParticipantAnswers = findAllQuizParticipantAnswers();
        for (QuizParticipantAnswer quizParticipantAnswer : quizParticipantAnswers) {
            deleteQuizParticipantAnswer(quizParticipantAnswer);
        }
    }
}
