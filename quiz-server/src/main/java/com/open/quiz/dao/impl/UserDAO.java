package com.open.quiz.dao.impl;

import com.open.quiz.dao.BaseDao;
import com.open.quiz.model.User;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;
import java.util.Optional;

@Repository
public class UserDAO extends BaseDao {

    public boolean existsByUsername(String username) {
        return findUserByUsername(username).isPresent();
    }

    public boolean existsByEmail(String email) {
        return findUserByUsername(email).isPresent();
    }

    public User createUser(User user) {
        em().persist(user);
        return user;
    }

    public Optional<User> findUserById(Long id) {
        return Optional.of(em().find(User.class, id));
    }

    public Optional<User> findUserByUsername(String username) {
        User user;
        try {
            user = em().createQuery("SELECT c FROM " + User.class.getSimpleName() + " c WHERE c.username = :username", User.class)
                    .setParameter("username", username).getSingleResult();
        } catch (NoResultException nrex) {
            return Optional.empty();
        }

        return Optional.of(user);
    }

    public Optional<User> findUserByEmail(String email) {
        User user;
        try {
            user = em().createQuery("SELECT c FROM " + User.class.getSimpleName() + " c WHERE c.email = :email", User.class)
                    .setParameter("email", email).getSingleResult();
        } catch (NoResultException nrex) {
            return Optional.empty();
        }

        return Optional.of(user);
    }

    public List<User> findAllUsers() {
        return em().createQuery("SELECT t FROM " + User.class.getSimpleName() + " t").getResultList();
    }

    public User updateUser(User user) {
        return em().merge(user);
    }

    public void deleteUserById(Long id) {
        em().remove(findUserById(id));
    }

    public void deleteUser(User user) {
        em().remove(user);
    }

    public void deleteAllUsers() {
        List<User> users = findAllUsers();
        for (User user : users) {
            deleteUser(user);
        }
    }
}
