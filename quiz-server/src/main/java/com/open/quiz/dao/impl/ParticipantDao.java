package com.open.quiz.dao.impl;

import com.open.quiz.dao.BaseDao;
import com.open.quiz.model.Participant;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ParticipantDao extends BaseDao {

    public Participant createParticipant(Participant participant) {
        em().persist(participant);
        return participant;
    }

    public Participant updateParticipant(Participant participant) {
        return em().merge(participant);
    }

    public Participant findParticipantById(Long id) {
        return em().find(Participant.class, id);
    }

    public void deleteParticipant(Participant participant) {
        em().remove(participant);
    }

    public void deleteParticipantById(Long id) {
        Participant participant = findParticipantById(id);
        deleteParticipant(participant);
    }

    public List<Participant> findAllParticipants() {
        return em().createQuery("SELECT t FROM " + Participant.class.getSimpleName() + " t").getResultList();
    }

    public void deleteAllParticipants() {
        List<Participant> participants = findAllParticipants();
        for (Participant participant : participants) {
            deleteParticipant(participant);
        }
    }
}
