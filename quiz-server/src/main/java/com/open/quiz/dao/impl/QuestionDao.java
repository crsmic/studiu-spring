package com.open.quiz.dao.impl;

import com.open.quiz.dao.BaseDao;
import com.open.quiz.model.Question;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class QuestionDao extends BaseDao {

    public Question createQuestion(Question question) {
        em().persist(question);
        return question;
    }

    public Question updateQuestion(Question question) {
        return em().merge(question);
    }

    public Question findQuestionById(Long id) {
        return em().find(Question.class, id);
    }

    public void deleteQuestion(Question question) {
        em().remove(question);
    }

    public void deleteQuestionById(Long id) {
        Question question = findQuestionById(id);
        deleteQuestion(question);
    }

    public List<Question> findAllQuestions() {
        return em().createQuery("SELECT t FROM " + Question.class.getSimpleName() + " t").getResultList();
    }

    public void deleteAllQuestions() {
        List<Question> questions = findAllQuestions();
        for (Question question : questions) {
            deleteQuestion(question);
        }
    }
}
