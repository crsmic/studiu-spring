package com.open.quiz.dao.impl;

import com.open.quiz.dao.BaseDao;
import com.open.quiz.model.Position;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PositionDao extends BaseDao {

    public Position createPosition(Position position) {
        em().persist(position);
        return position;
    }

    public Position updatePosition(Position position) {
        return em().merge(position);
    }

    public Position findPositionById(Long id) {
        return em().find(Position.class, id);
    }

    public void deletePosition(Position position) {
        em().remove(position);
    }

    public void deletePositionById(Long id) {
        Position position = findPositionById(id);
        deletePosition(position);
    }

    public List<Position> findAllPositions() {
        return em().createQuery("SELECT t FROM " + Position.class.getSimpleName() + " t").getResultList();
    }

    public void deleteAllPositions() {
        List<Position> positions = findAllPositions();
        for (Position position : positions) {
            deletePosition(position);
        }
    }
}
