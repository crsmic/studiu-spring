package com.open.quiz.dao.impl;

import com.open.quiz.dao.BaseDao;
import com.open.quiz.model.CategoryType;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CategoryTypeDao extends BaseDao {

    public CategoryType createCategoryType(CategoryType categoryType) {
        em().persist(categoryType);
        return categoryType;
    }

    public CategoryType updateCategoryType(CategoryType categoryType) {
        return em().merge(categoryType);
    }

    public CategoryType findCategoryTypeById(Long id) {
        return em().find(CategoryType.class, id);
    }

    public void deleteCategoryType(CategoryType categoryType) {
        em().remove(categoryType);
    }

    public void deleteCategoryTypeById(Long id) {
        CategoryType categoryType = findCategoryTypeById(id);
        deleteCategoryType(categoryType);
    }

    public List<CategoryType> findAllCategoryTypes() {
        return em().createQuery("SELECT t FROM " + CategoryType.class.getSimpleName() + " t").getResultList();
    }

    public void deleteAllCategoryTipes(){
        List<CategoryType> categoryTypes = findAllCategoryTypes();
        for (CategoryType categoryType : categoryTypes) {
            deleteCategoryType(categoryType);
        }
    }
}
