package com.open.quiz.dao.impl;

import com.open.quiz.dao.BaseDao;
import com.open.quiz.model.Category;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CategoryDao extends BaseDao {

    public Category createCategory(Category category) {
        em().persist(category);
        return category;
    }

    public Category updateCategory(Category category) {
        return em().merge(category);
    }

    public Category findCategoryById(Long id) {
        return em().find(Category.class, id);
    }

    public void deleteCategory(Category category) {
        em().remove(category);
    }

    public void deleteCategoryById(Long id) {
        Category category = findCategoryById(id);
        deleteCategory(category);
    }

    public List<Category> findAllCategories() {
        return em().createQuery("SELECT t FROM " + Category.class.getSimpleName() + " t").getResultList();
    }

    public void deleteAllCategories() {
        List<Category> categories = findAllCategories();
        for (Category category : categories) {
            deleteCategory(category);
        }
    }
}
