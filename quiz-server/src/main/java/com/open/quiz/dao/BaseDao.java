package com.open.quiz.dao;

import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class BaseDao {

    @PersistenceContext
    private EntityManager em;

    protected EntityManager em() {
        return em;
    }
}
