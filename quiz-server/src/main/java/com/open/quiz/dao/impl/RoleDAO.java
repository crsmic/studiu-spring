package com.open.quiz.dao.impl;

import com.open.quiz.dao.BaseDao;
import com.open.quiz.model.Role;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

@Repository
public class RoleDAO extends BaseDao {

    public Role createRole(Role role) {
        em().persist(role);
        return role;
    }

    public Role updateRole(Role role) {
        return em().merge(role);
    }

    public Role findRoleById(Long id) {
        return em().find(Role.class, id);
    }

    public Optional<Role> findRoleByName(String role) {
        TypedQuery<Role> query = em().createQuery("SELECT c FROM " + Role.class.getSimpleName() + " c WHERE c.role = :role", Role.class);
        return Optional.of(query.setParameter("role", role).getSingleResult());
    }

    public List<Role> findAllRoles() {
        return em().createQuery("SELECT t FROM " + Role.class.getSimpleName() + " t").getResultList();
    }

    public void deleteRoleById(Long id) {
        em().remove(findRoleById(id));
    }

    public void deleteRole(Role role) {
        em().remove(role);
    }

    public void deleteAllRoles() {
        List<Role> roles = findAllRoles();
        for (Role role : roles) {
            deleteRole(role);
        }
    }
}
