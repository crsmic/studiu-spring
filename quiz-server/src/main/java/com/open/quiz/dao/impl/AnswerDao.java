package com.open.quiz.dao.impl;

import com.open.quiz.dao.BaseDao;
import com.open.quiz.model.Answer;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AnswerDao extends BaseDao {

    public Answer createAnswer(Answer answer) {
        em().persist(answer);
        return answer;
    }

    public Answer updateAnswer(Answer answer) {
        return em().merge(answer);
    }

    public Answer findAnswerById(Long id) {
        return em().find(Answer.class, id);
    }

    public void deleteAnswer(Answer answer) {
        em().remove(answer);
    }

    public void deleteAnswerById(Long id) {
        Answer answer = findAnswerById(id);
        deleteAnswer(answer);
    }

    public List<Answer> findAllAnswers() {
        return em().createQuery("SELECT t FROM " + Answer.class.getSimpleName() + " t").getResultList();
    }

    public void deleteAllAnswers() {
        List<Answer> answers = findAllAnswers();
        for (Answer answer : answers) {
            deleteAnswer(answer);
        }
    }
}
