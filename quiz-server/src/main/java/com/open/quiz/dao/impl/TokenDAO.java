package com.open.quiz.dao.impl;

import com.open.quiz.dao.BaseDao;
import com.open.quiz.model.Token;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TokenDAO extends BaseDao {

    public Token createToken(Token token) {
        em().persist(token);
        return token;
    }

    public Token updateToken(Token token) {
        return em().merge(token);
    }

    public Token findTokenById(Long id) {
        return em().find(Token.class, id);
    }

    public void deleteToken(Token token) {
        em().remove(token);
    }

    public void deleteTokenById(Long id) {
        em().remove(findTokenById(id));
    }

    public List<Token> findAllTokens() {
        return em().createQuery("SELECT t FROM " + Token.class.getSimpleName() + " t").getResultList();
    }

    public void deleteAllTokens() {
        List<Token> tokens = findAllTokens();
        for (Token token : tokens) {
            deleteToken(token);
        }
    }

}