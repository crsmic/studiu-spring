package com.open.quiz.service.dto;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

public class PrivilegeDTO implements Serializable {

    private Long id;

    private String name;

    private Set<RoleDTO> roles;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<RoleDTO> getRoles() {
        return roles;
    }

    public void setRoles(Set<RoleDTO> roles) {
        this.roles = roles;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PrivilegeDTO that = (PrivilegeDTO) o;
        return id.equals(that.id) &&
                name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public String toString() {
        return "PrivilegesDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
