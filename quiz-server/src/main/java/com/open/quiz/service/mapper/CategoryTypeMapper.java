package com.open.quiz.service.mapper;

import com.open.quiz.model.CategoryType;
import com.open.quiz.service.dto.CategoryTypeDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {CategoryMapper.class})
public interface CategoryTypeMapper extends EntityMapper<CategoryTypeDTO, CategoryType> {

    default CategoryType fromId(Long id) {
        if (id == null) {
            return null;
        }
        CategoryType categoryType = new CategoryType();
        categoryType.setId(id);
        return categoryType;
    }
}
