package com.open.quiz.service;

import com.open.quiz.dao.impl.AnswerDao;
import com.open.quiz.model.Answer;
import com.open.quiz.service.dto.AnswerDTO;
import com.open.quiz.service.mapper.AnswerMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class AnswerService {

    @Autowired
    private AnswerDao answerDao;

    @Autowired
    private AnswerMapper answerMapper;

    public AnswerDTO createAnswer(AnswerDTO answerDTO){
        Answer answer = answerMapper.toEntity(answerDTO);
        answer = answerDao.createAnswer(answer);
        return answerMapper.toDTO(answer);
    }

    public AnswerDTO updateAnswer(AnswerDTO answerDTO){
        Answer answer = answerMapper.toEntity(answerDTO);
        answer = answerDao.updateAnswer(answer);
        return answerMapper.toDTO(answer);
    }

    @Transactional(readOnly = true)
    public AnswerDTO findAnswerById(Long id) {
        Answer answer = answerDao.findAnswerById(id);
        return answerMapper.toDTO(answer);
    }

    public void deleteAnswer(AnswerDTO answerDTO) {
        Answer answer = answerMapper.toEntity(answerDTO);
        answerDao.deleteAnswer(answer);
    }

    public void deleteAnswerById(Long id) {
        answerDao.deleteAnswerById(id);
    }

    @Transactional(readOnly = true)
    public List<AnswerDTO> findAllAnswers() {
        List<Answer> answers = answerDao.findAllAnswers();
        return answerMapper.toDTOs(answers);
    }

    public void deleteAllAnswers() {
        answerDao.deleteAllAnswers();
    }
}
