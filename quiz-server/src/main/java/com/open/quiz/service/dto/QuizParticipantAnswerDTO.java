package com.open.quiz.service.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

public class QuizParticipantAnswerDTO implements Serializable {

    private Long id;

    @NotNull
    private Long givenAnswerId;

    private LocalDateTime startDate;

    private LocalDateTime endDate;

    private Long quizParticipantId;

    private Long questionId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGivenAnswerId() {
        return givenAnswerId;
    }

    public void setGivenAnswerId(Long givenAnswerId) {
        this.givenAnswerId = givenAnswerId;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public Long getQuizParticipantId() {
        return quizParticipantId;
    }

    public void setQuizParticipantId(Long quizParticipantId) {
        this.quizParticipantId = quizParticipantId;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof QuizParticipantAnswerDTO)) return false;
        QuizParticipantAnswerDTO that = (QuizParticipantAnswerDTO) o;
        return Objects.equals(getId(), that.getId()) &&
            Objects.equals(getGivenAnswerId(), that.getGivenAnswerId()) &&
            Objects.equals(getStartDate(), that.getStartDate()) &&
            Objects.equals(getEndDate(), that.getEndDate()) &&
            Objects.equals(getQuizParticipantId(), that.getQuizParticipantId()) &&
            Objects.equals(getQuestionId(), that.getQuestionId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getGivenAnswerId(), getStartDate(), getEndDate(), getQuizParticipantId(), getQuestionId());
    }

    @Override
    public String toString() {
        return "QuizParticipantAnswerDTO{" +
            "id=" + id +
            ", givenAnswerId=" + givenAnswerId +
            ", startDate=" + startDate +
            ", endDate=" + endDate +
            ", quizParticipantId=" + quizParticipantId +
            ", questionId=" + questionId +
            '}';
    }
}
