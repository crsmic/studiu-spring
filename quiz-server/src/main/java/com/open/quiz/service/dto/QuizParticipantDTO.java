package com.open.quiz.service.dto;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class QuizParticipantDTO implements Serializable {

    private Long id;

    @Size(max = 20)
    private String result;

    private LocalDateTime startDate;

    private LocalDateTime endDate;

    private Long quizId;

    private Long participantId;

    private Set<QuizParticipantAnswerDTO> quizParticipantAnswerDTOs = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public Long getQuizId() {
        return quizId;
    }

    public void setQuizId(Long quizId) {
        this.quizId = quizId;
    }

    public Long getParticipantId() {
        return participantId;
    }

    public void setParticipantId(Long participantId) {
        this.participantId = participantId;
    }

    public Set<QuizParticipantAnswerDTO> getQuizParticipantAnswerDTOs() {
        return quizParticipantAnswerDTOs;
    }

    public void setQuizParticipantAnswerDTOs(Set<QuizParticipantAnswerDTO> quizParticipantAnswerDTOs) {
        this.quizParticipantAnswerDTOs = quizParticipantAnswerDTOs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof QuizParticipantDTO)) return false;
        QuizParticipantDTO that = (QuizParticipantDTO) o;
        return Objects.equals(getId(), that.getId()) &&
            Objects.equals(getResult(), that.getResult()) &&
            Objects.equals(getStartDate(), that.getStartDate()) &&
            Objects.equals(getEndDate(), that.getEndDate()) &&
            Objects.equals(getQuizId(), that.getQuizId()) &&
            Objects.equals(getParticipantId(), that.getParticipantId()) &&
            Objects.equals(getQuizParticipantAnswerDTOs(), that.getQuizParticipantAnswerDTOs());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getResult(), getStartDate(), getEndDate(), getQuizId(), getParticipantId(), getQuizParticipantAnswerDTOs());
    }

    @Override
    public String toString() {
        return "QuizParticipantDTO{" +
            "id=" + id +
            ", result='" + result + '\'' +
            ", startDate=" + startDate +
            ", endDate=" + endDate +
            ", quizId=" + quizId +
            ", participantId=" + participantId +
            ", quizParticipantAnswerDTOs=" + quizParticipantAnswerDTOs +
            '}';
    }
}
