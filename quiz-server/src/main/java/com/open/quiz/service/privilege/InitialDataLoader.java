package com.open.quiz.service.privilege;

import com.open.quiz.dao.impl.PrivilegeDAO;
import com.open.quiz.dao.impl.RoleDAO;
import com.open.quiz.dao.impl.UserDAO;
import com.open.quiz.model.Privilege;
import com.open.quiz.model.Role;
import com.open.quiz.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Set;

@Component
public class InitialDataLoader implements
        ApplicationListener<ContextRefreshedEvent> {
    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

    }

//    boolean alreadySetup = false;
//
//    @Autowired
//    private UserDAO userDAO;
//
//    @Autowired
//    private RoleDAO roleDAO;
//
//    @Autowired
//    private PrivilegeDAO privilegeDAO;
//
////    @Autowired
////    private PasswordEncoder passwordEncoder;
//
//    @Override
//    @Transactional
//    public void onApplicationEvent(ContextRefreshedEvent event) {
//
//        if (alreadySetup)
//            return;
//        Privilege readPrivilege
//                = createPrivilegeIfNotFound("READ_PRIVILEGE");
//        Privilege writePrivilege
//                = createPrivilegeIfNotFound("WRITE_PRIVILEGE");
//
//        Set<Privilege> adminPrivileges = new HashSet();
//        adminPrivileges.add(readPrivilege);
//        adminPrivileges.add(writePrivilege);
//        Set<Privilege> readPrivileges = new HashSet();
//        readPrivileges.add(readPrivilege);
//        createRoleIfNotFound("ROLE_ADMIN", adminPrivileges);
//        createRoleIfNotFound("ROLE_USER", readPrivileges);
//
//        Role adminRole = roleDAO.findRoleByName("ROLE_ADMIN");
//        User user = new User();
//        user.setFirstName("Test-firstname");
//        user.setLastName("Test-lastname");
//        user.setUsername("Test-username");
//        user.setEmail("test@test.com");
//        user.setPhone("test-phone");
//        user.setPassword("test-password");
//
//        if (user.getUserRoles() == null) {
//            user.setUserRoles(new HashSet<>());
//        }
//        user.getUserRoles().add(adminRole);
////        user.setEnabled(true);
//        userDAO.createUser(user);
//
//        alreadySetup = true;
//    }
//
//    @Transactional
//    private Privilege createPrivilegeIfNotFound(String name) {
//
//        Privilege privilege = privilegeDAO.findPrivilegeByName(name);
//        if (privilege == null) {
//            privilege = new Privilege();
//            privilege.setName(name);
//            privilegeDAO.createPrivilege(privilege);
//        }
//        return privilege;
//    }
//
//    @Transactional
//    private Role createRoleIfNotFound(
//            String name, Set<Privilege> privileges) {
//
//        Role role = roleDAO.findRoleByName(name);
//        if (role == null) {
//            role = new Role();
//            role.setRole(name);
//            role.setRolePrivileges(privileges);
//            roleDAO.createRole(role);
//        }
//        return role;
//    }
}
