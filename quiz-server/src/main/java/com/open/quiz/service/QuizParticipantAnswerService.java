package com.open.quiz.service;

import com.open.quiz.dao.impl.QuizParticipantAnswerDao;
import com.open.quiz.model.QuizParticipantAnswer;
import com.open.quiz.service.dto.QuizParticipantAnswerDTO;
import com.open.quiz.service.mapper.QuizParticipantAnswerMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class QuizParticipantAnswerService {

    @Autowired
    private QuizParticipantAnswerDao participantAnswerDao;

    @Autowired
    private QuizParticipantAnswerMapper participantAnswerMapper;

    public QuizParticipantAnswerDTO createQuizParticipantAnswer(QuizParticipantAnswerDTO participantAnswerDTO){
        QuizParticipantAnswer entity = participantAnswerMapper.toEntity(participantAnswerDTO);
        entity = participantAnswerDao.createQuizParticipantAnswer(entity);
        return participantAnswerMapper.toDTO(entity);
    }

    public QuizParticipantAnswerDTO updateQuizParticipantAnswer(QuizParticipantAnswerDTO participantAnswerDTO){
        QuizParticipantAnswer entity = participantAnswerMapper.toEntity(participantAnswerDTO);
        entity = participantAnswerDao.updateQuizParticipantAnswer(entity);
        return participantAnswerMapper.toDTO(entity);
    }

    @Transactional(readOnly = true)
    public QuizParticipantAnswerDTO findQuizParticipantAnswerById(Long id) {
        QuizParticipantAnswer entity = participantAnswerDao.findQuizParticipantAnswerById(id);
        return participantAnswerMapper.toDTO(entity);
    }

    public void deleteQuizParticipantAnswer(QuizParticipantAnswerDTO participantAnswerDTO) {
        QuizParticipantAnswer entity = participantAnswerMapper.toEntity(participantAnswerDTO);
        participantAnswerDao.deleteQuizParticipantAnswer(entity);
    }

    public void deleteQuizParticipantAnswerById(Long id) {
        participantAnswerDao.deleteQuizParticipantAnswerById(id);
    }

    @Transactional(readOnly = true)
    public List<QuizParticipantAnswerDTO> findAllQuizParticipantAnswers() {
        List<QuizParticipantAnswer> participantAnswers = participantAnswerDao.findAllQuizParticipantAnswers();
        return participantAnswerMapper.toDTOs(participantAnswers);
    }

    public void deleteAllQuizParticipantAnswers() {
        participantAnswerDao.deleteAllQuizParticipantAnswers();
    }
}
