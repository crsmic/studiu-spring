package com.open.quiz.service.dto;

import com.open.quiz.model.enumeration.Difficulty;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class QuestionDTO implements Serializable {

    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Difficulty questionDifficulty;

    @NotNull
    @Size(min = 3, max = 1000)
    private String questionText;

    @NotNull
    private Long questionMultiAnswer;

    private Long categoryId;

    private Set<AnswerDTO> answerDTOs = new HashSet<>();

    private Set<QuizParticipantAnswerDTO> quizParticipantAnswerDTOs = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Difficulty getQuestionDifficulty() {
        return questionDifficulty;
    }

    public void setQuestionDifficulty(Difficulty questionDifficulty) {
        this.questionDifficulty = questionDifficulty;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public Long getQuestionMultiAnswer() {
        return questionMultiAnswer;
    }

    public void setQuestionMultiAnswer(Long questionMultiAnswer) {
        this.questionMultiAnswer = questionMultiAnswer;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Set<AnswerDTO> getAnswerDTOs() {
        return answerDTOs;
    }

    public void setAnswerDTOs(Set<AnswerDTO> answerDTOs) {
        this.answerDTOs = answerDTOs;
    }

    public Set<QuizParticipantAnswerDTO> getQuizParticipantAnswerDTOs() {
        return quizParticipantAnswerDTOs;
    }

    public void setQuizParticipantAnswerDTOs(Set<QuizParticipantAnswerDTO> quizParticipantAnswerDTOs) {
        this.quizParticipantAnswerDTOs = quizParticipantAnswerDTOs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof QuestionDTO)) return false;
        QuestionDTO that = (QuestionDTO) o;
        return Objects.equals(getId(), that.getId()) &&
            getQuestionDifficulty() == that.getQuestionDifficulty() &&
            Objects.equals(getQuestionText(), that.getQuestionText()) &&
            Objects.equals(getQuestionMultiAnswer(), that.getQuestionMultiAnswer()) &&
            Objects.equals(getCategoryId(), that.getCategoryId()) &&
            Objects.equals(getAnswerDTOs(), that.getAnswerDTOs()) &&
            Objects.equals(getQuizParticipantAnswerDTOs(), that.getQuizParticipantAnswerDTOs());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getQuestionDifficulty(), getQuestionText(), getQuestionMultiAnswer(), getCategoryId(), getAnswerDTOs(), getQuizParticipantAnswerDTOs());
    }

    @Override
    public String toString() {
        return "QuestionDTO{" +
            "id=" + id +
            ", questionDifficulty=" + questionDifficulty +
            ", questionText='" + questionText + '\'' +
            ", questionMultiAnswer=" + questionMultiAnswer +
            ", categoryId=" + categoryId +
            ", answerDTOs=" + answerDTOs +
            ", quizParticipantAnswerDTOs=" + quizParticipantAnswerDTOs +
            '}';
    }
}
