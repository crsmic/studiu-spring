package com.open.quiz.service;

import com.open.quiz.dao.impl.PositionDao;
import com.open.quiz.model.Position;
import com.open.quiz.service.dto.PositionDTO;
import com.open.quiz.service.mapper.PositionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PositionService {

    @Autowired
    private PositionDao positionDao;

    @Autowired
    private PositionMapper positionMapper;

    public PositionDTO createPosition(PositionDTO positionDTO) {
        Position position = positionMapper.toEntity(positionDTO);
        position = positionDao.createPosition(position);
        return positionMapper.toDTO(position);
    }

    public PositionDTO updatePosition(PositionDTO positionDTO) {
        Position position = positionMapper.toEntity(positionDTO);
        position = positionDao.updatePosition(position);
        return positionMapper.toDTO(position);
    }

    @Transactional(readOnly = true)
    public PositionDTO findPositionById(Long id) {
        Position position = positionDao.findPositionById(id);
        return positionMapper.toDTO(position);
    }

    public void deletePosition(PositionDTO positionDTO) {
        Position position = positionMapper.toEntity(positionDTO);
        positionDao.deletePosition(position);
    }

    public void deletePositionById(Long id) {
        positionDao.deletePositionById(id);
    }

    @Transactional(readOnly = true)
    public List<PositionDTO> findAllPositions() {
        List<Position> positions = positionDao.findAllPositions();
        return positionMapper.toDTOs(positions);
    }

    public void deleteAllPositions() {
        positionDao.deleteAllPositions();
    }
}
