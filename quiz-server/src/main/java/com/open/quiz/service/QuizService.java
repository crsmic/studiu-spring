package com.open.quiz.service;

import com.open.quiz.dao.impl.QuizDao;
import com.open.quiz.model.Quiz;
import com.open.quiz.service.dto.QuizDTO;
import com.open.quiz.service.mapper.QuizMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class QuizService {

    @Autowired
    private QuizDao quizDao;

    @Autowired
    private QuizMapper quizMapper;

    public QuizDTO createQuiz(QuizDTO quizDTO){
        Quiz entity = quizMapper.toEntity(quizDTO);
        entity = quizDao.createQuiz(entity);
        return quizMapper.toDTO(entity);
    }

    public QuizDTO updateQuiz(QuizDTO quizDTO){
        Quiz entity = quizMapper.toEntity(quizDTO);
        entity = quizDao.updateQuiz(entity);
        return quizMapper.toDTO(entity);
    }

    @Transactional(readOnly = true)
    public QuizDTO findQuizById(Long id) {
        Quiz entity = quizDao.findQuizById(id);
        return quizMapper.toDTO(entity);
    }

    public void deleteQuiz(QuizDTO quizDTO) {
        Quiz entity = quizMapper.toEntity(quizDTO);
        quizDao.deleteQuiz(entity);
    }

    public void deleteQuizById(Long id) {
        quizDao.deleteQuizById(id);
    }

    @Transactional(readOnly = true)
    public List<QuizDTO> findAllQuizs() {
        List<Quiz> allQuizs = quizDao.findAllQuizs();
        return quizMapper.toDTOs(allQuizs);
    }

    public void deleteAllQuizs() {
        quizDao.deleteAllQuizs();
    }
}
