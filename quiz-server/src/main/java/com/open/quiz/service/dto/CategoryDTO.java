package com.open.quiz.service.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class CategoryDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    private Long categoryTypeId;

    private Long parentId;

    private Set<QuestionDTO> questionDTOs = new HashSet<>();

    private Set<CategoryDTO> subCategoryDTOs = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCategoryTypeId() {
        return categoryTypeId;
    }

    public void setCategoryTypeId(Long categoryTypeId) {
        this.categoryTypeId = categoryTypeId;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Set<QuestionDTO> getQuestionDTOs() {
        return questionDTOs;
    }

    public void setQuestionDTOs(Set<QuestionDTO> questionDTOs) {
        this.questionDTOs = questionDTOs;
    }

    public Set<CategoryDTO> getSubCategoryDTOs() {
        return subCategoryDTOs;
    }

    public void setSubCategoryDTOs(Set<CategoryDTO> subCategoryDTOs) {
        this.subCategoryDTOs = subCategoryDTOs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CategoryDTO)) return false;
        CategoryDTO that = (CategoryDTO) o;
        return Objects.equals(getId(), that.getId()) &&
            Objects.equals(getName(), that.getName()) &&
            Objects.equals(getCategoryTypeId(), that.getCategoryTypeId()) &&
            Objects.equals(getParentId(), that.getParentId()) &&
            Objects.equals(getQuestionDTOs(), that.getQuestionDTOs()) &&
            Objects.equals(getSubCategoryDTOs(), that.getSubCategoryDTOs());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getCategoryTypeId(), getParentId(), getQuestionDTOs(), getSubCategoryDTOs());
    }

    @Override
    public String toString() {
        return "CategoryDTO{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", categoryTypeId=" + categoryTypeId +
            ", parentId=" + parentId +
            ", questionDTOs=" + questionDTOs +
            ", subCategoryDTOs=" + subCategoryDTOs +
            '}';
    }
}
