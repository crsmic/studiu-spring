package com.open.quiz.service.mapper;

import com.open.quiz.model.Category;
import com.open.quiz.service.dto.CategoryDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {CategoryTypeMapper.class, QuestionMapper.class})
public interface CategoryMapper extends EntityMapper<CategoryDTO, Category> {

    @Mapping(source = "categoryType.id", target = "categoryTypeId")
    @Mapping(source = "parent.id", target = "parentId")
    CategoryDTO toDTO(Category category);

    @Mapping(source = "categoryTypeId", target = "categoryType")
    @Mapping(source = "parentId", target = "parent")
    Category toEntity(CategoryDTO categoryDTO);

    default Category fromId(Long id) {
        if (id == null) {
            return null;
        }
        Category category = new Category();
        category.setId(id);
        return category;
    }
}
