package com.open.quiz.service;

import com.open.quiz.dao.impl.QuestionDao;
import com.open.quiz.model.Question;
import com.open.quiz.service.dto.QuestionDTO;
import com.open.quiz.service.mapper.QuestionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class QuestionService {

    @Autowired
    private QuestionDao questionDao;

    @Autowired
    private QuestionMapper questionMapper;

    public QuestionDTO createQuestion(QuestionDTO questionDTO){
        Question question = questionMapper.toEntity(questionDTO);
        question = questionDao.createQuestion(question);
        return questionMapper.toDTO(question);
    }

    public QuestionDTO updateQuestion(QuestionDTO questionDTO){
        Question question = questionMapper.toEntity(questionDTO);
        question = questionDao.updateQuestion(question);
        return questionMapper.toDTO(question);
    }

    @Transactional(readOnly = true)
    public QuestionDTO findQuestionById(Long id) {
        Question question = questionDao.findQuestionById(id);
        return questionMapper.toDTO(question);
    }

    public void deleteQuestion(QuestionDTO questionDTO) {
        Question question = questionMapper.toEntity(questionDTO);
        questionDao.deleteQuestion(question);
    }

    public void deleteQuestionById(Long id) {
        questionDao.deleteQuestionById(id);
    }

    @Transactional(readOnly = true)
    public List<QuestionDTO> findAllQuestions() {
        List<Question> questions = questionDao.findAllQuestions();
        return questionMapper.toDTOs(questions);
    }

    public void deleteAllQuestions() {
        questionDao.deleteAllQuestions();
    }
}
