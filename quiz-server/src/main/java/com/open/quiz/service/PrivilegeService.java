package com.open.quiz.service;

import com.open.quiz.dao.impl.PrivilegeDAO;
import com.open.quiz.model.Privilege;
import com.open.quiz.service.dto.PrivilegeDTO;
import com.open.quiz.service.mapper.PrivilegeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PrivilegeService {

    @Autowired
    private PrivilegeDAO privilegeDAO;

    @Autowired
    private PrivilegeMapper privilegeMapper;

    public PrivilegeDTO createPrivilege(PrivilegeDTO privilegeDTO) {
        Privilege privilege = privilegeMapper.toEntity(privilegeDTO);
        privilege = privilegeDAO.createPrivilege(privilege);
        return privilegeMapper.toDTO(privilege);
    }

    @Transactional(readOnly = true)
    public PrivilegeDTO findPrivilegeById(Long id) {
        Privilege privilege = privilegeDAO.findPrivilegeById(id);
        return privilegeMapper.toDTO(privilege);
    }

    @Transactional(readOnly = true)
    public PrivilegeDTO findPrivilegeByName(String name) {
        Privilege privilege = privilegeDAO.findPrivilegeByName(name);
        return privilegeMapper.toDTO(privilege);
    }

    @Transactional(readOnly = true)
    public List<PrivilegeDTO> findAllPrivileges() {
        List<Privilege> privileges = privilegeDAO.findAllPrivileges();
        return privilegeMapper.toDTOs(privileges);
    }

    public PrivilegeDTO update(PrivilegeDTO privilegeDTO) {
        Privilege privilege = privilegeMapper.toEntity(privilegeDTO);
        privilege = privilegeDAO.updatePrivilege(privilege);
        return privilegeMapper.toDTO(privilege);
    }

    public void deletePrivilegeById(Long id) {
        privilegeDAO.deletePrivilegeById(id);
    }

    public void deleteAllPrivileges() {
        privilegeDAO.deleteAllPrivileges();
    }

}
