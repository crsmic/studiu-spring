package com.open.quiz.service.mapper;

import com.open.quiz.model.Position;
import com.open.quiz.service.dto.PositionDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {QuizMapper.class})
public interface PositionMapper extends EntityMapper<PositionDTO, Position> {

    default Position fromId(Long id) {
        if (id == null) {
            return null;
        }
        Position position = new Position();
        position.setId(id);
        return position;
    }
}
