package com.open.quiz.service;

import com.open.quiz.dao.impl.QuizParticipantDao;
import com.open.quiz.model.QuizParticipant;
import com.open.quiz.service.dto.QuizParticipantDTO;
import com.open.quiz.service.mapper.QuizParticipantMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class QuizParticipantService {

    @Autowired
    private QuizParticipantDao quizParticipantDao;

    @Autowired
    private QuizParticipantMapper quizParticipantMapper;

    public QuizParticipantDTO createQuizParticipant(QuizParticipantDTO quizParticipantDTO){
        QuizParticipant entity = quizParticipantMapper.toEntity(quizParticipantDTO);
        entity = quizParticipantDao.createQuizParticipant(entity);
        return quizParticipantMapper.toDTO(entity);
    }

    public QuizParticipantDTO updateQuizParticipant(QuizParticipantDTO quizParticipantDTO){
        QuizParticipant entity = quizParticipantMapper.toEntity(quizParticipantDTO);
        entity = quizParticipantDao.updateQuizParticipant(entity);
        return quizParticipantMapper.toDTO(entity);
    }

    @Transactional(readOnly = true)
    public QuizParticipantDTO findQuizParticipantById(Long id) {
        QuizParticipant entity = quizParticipantDao.findQuizParticipantById(id);
        return quizParticipantMapper.toDTO(entity);
    }

    public void deleteQuizParticipant(QuizParticipantDTO quizParticipantDTO) {
        QuizParticipant entity = quizParticipantMapper.toEntity(quizParticipantDTO);
        quizParticipantDao.deleteQuizParticipant(entity);
    }

    public void deleteQuizParticipantById(Long id) {
        quizParticipantDao.deleteQuizParticipantById(id);
    }

    @Transactional(readOnly = true)
    public List<QuizParticipantDTO> findAllQuizParticipants() {
        List<QuizParticipant> participants = quizParticipantDao.findAllQuizParticipants();
        return quizParticipantMapper.toDTOs(participants);
    }

    public void deleteAllQuizParticipantss() {
        quizParticipantDao.deleteAllQuizParticipants();
    }
}
