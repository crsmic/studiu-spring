package com.open.quiz.service.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

public class TokenDTO implements Serializable {

    private Long id;

    private String token;

    private int timeOfSolving;

    private LocalDateTime valability;

    private Long participantId;

    private Long quizId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getTimeOfSolving() {
        return timeOfSolving;
    }

    public void setTimeOfSolving(int timeOfSolving) {
        this.timeOfSolving = timeOfSolving;
    }

    public LocalDateTime getValability() {
        return valability;
    }

    public void setValability(LocalDateTime valability) {
        this.valability = valability;
    }

    public Long getParticipantId() {
        return participantId;
    }

    public void setParticipantId(Long participantId) {
        this.participantId = participantId;
    }

    public Long getQuizId() {
        return quizId;
    }

    public void setQuizId(Long quizId) {
        this.quizId = quizId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TokenDTO tokenDTO = (TokenDTO) o;
        return timeOfSolving == tokenDTO.timeOfSolving &&
                id.equals(tokenDTO.id) &&
                token.equals(tokenDTO.token) &&
                valability.equals(tokenDTO.valability);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, token, timeOfSolving, valability);
    }

    @Override
    public String toString() {
        return "TokenDTO{" +
                "id=" + id +
                ", token='" + token + '\'' +
                ", timeOfSolving=" + timeOfSolving +
                ", valability=" + valability +
                '}';
    }
}
