package com.open.quiz.service.mapper;

import com.open.quiz.model.Quiz;
import com.open.quiz.service.dto.QuizDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {QuizParticipantMapper.class, PositionMapper.class})
public interface QuizMapper extends EntityMapper<QuizDTO, Quiz> {

    @Mapping(source = "position.id", target = "positionId")
    QuizDTO toDTO(Quiz quiz);

    @Mapping(source = "positionId", target = "position")
    Quiz toEntity(QuizDTO quizDTO);

    default Quiz fromId(Long id) {
        if (id == null) {
            return null;
        }
        Quiz quiz = new Quiz();
        quiz.setId(id);
        return quiz;
    }
}
