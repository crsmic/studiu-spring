package com.open.quiz.service.mapper;

import com.open.quiz.model.Question;
import com.open.quiz.service.dto.QuestionDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {AnswerMapper.class, QuizParticipantAnswerMapper.class, CategoryMapper.class})
public interface QuestionMapper extends EntityMapper<QuestionDTO, Question> {

    @Mapping(source = "category.id", target = "categoryId")
    QuestionDTO toDTO(Question question);

    @Mapping(source = "categoryId", target = "category")
    Question toEntity(QuestionDTO questionDTO);

    default Question fromId(Long id) {
        if (id == null) {
            return null;
        }
        Question question = new Question();
        question.setId(id);
        return question;
    }
}
