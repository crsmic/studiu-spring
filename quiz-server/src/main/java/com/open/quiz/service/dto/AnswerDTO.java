package com.open.quiz.service.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

public class AnswerDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(min = 1, max = 1000)
    private String answerText;

    @NotNull
    private Boolean answerCorrectness;

    private Long questionId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAnswerText() {
        return answerText;
    }

    public void setAnswerText(String answerText) {
        this.answerText = answerText;
    }

    public Boolean getAnswerCorrectness() {
        return answerCorrectness;
    }

    public void setAnswerCorrectness(Boolean answerCorrectness) {
        this.answerCorrectness = answerCorrectness;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AnswerDTO)) return false;
        AnswerDTO answerDTO = (AnswerDTO) o;
        return Objects.equals(getId(), answerDTO.getId()) &&
            Objects.equals(getAnswerText(), answerDTO.getAnswerText()) &&
            Objects.equals(getAnswerCorrectness(), answerDTO.getAnswerCorrectness()) &&
            Objects.equals(getQuestionId(), answerDTO.getQuestionId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getAnswerText(), getAnswerCorrectness(), getQuestionId());
    }

    @Override
    public String toString() {
        return "AnswerDTO{" +
            "id=" + id +
            ", answerText='" + answerText + '\'' +
            ", answerCorrectness=" + answerCorrectness +
            ", questionId=" + questionId +
            '}';
    }
}
