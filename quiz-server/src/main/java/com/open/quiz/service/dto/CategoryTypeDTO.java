package com.open.quiz.service.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class CategoryTypeDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    private Set<CategoryDTO> categoryDTOs = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<CategoryDTO> getCategoryDTOs() {
        return categoryDTOs;
    }

    public void setCategoryDTOs(Set<CategoryDTO> categoryDTOs) {
        this.categoryDTOs = categoryDTOs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CategoryTypeDTO)) return false;
        CategoryTypeDTO that = (CategoryTypeDTO) o;
        return Objects.equals(getId(), that.getId()) &&
            Objects.equals(getName(), that.getName()) &&
            Objects.equals(getCategoryDTOs(), that.getCategoryDTOs());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getCategoryDTOs());
    }

    @Override
    public String toString() {
        return "CategoryTypeDTO{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", categoryDTOs=" + categoryDTOs +
            '}';
    }
}
