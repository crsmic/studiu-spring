package com.open.quiz.service;

import com.open.quiz.dao.impl.CategoryDao;
import com.open.quiz.model.Category;
import com.open.quiz.service.dto.CategoryDTO;
import com.open.quiz.service.mapper.CategoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CategoryService {

    @Autowired
    private CategoryDao categoryDao;

    @Autowired
    private CategoryMapper categoryMapper;

    public CategoryDTO createCategory(CategoryDTO categoryDTO){
        Category category = categoryMapper.toEntity(categoryDTO);
        category = categoryDao.createCategory(category);
        return categoryMapper.toDTO(category);
    }

    public CategoryDTO updateCategory(CategoryDTO categoryDTO){
        Category category = categoryMapper.toEntity(categoryDTO);
        category = categoryDao.updateCategory(category);
        return categoryMapper.toDTO(category);
    }

    @Transactional(readOnly = true)
    public CategoryDTO findCategoryById(Long id) {
        Category category = categoryDao.findCategoryById(id);
        return categoryMapper.toDTO(category);
    }

    public void deleteCategory(CategoryDTO categoryDTO) {
        Category category = categoryMapper.toEntity(categoryDTO);
        categoryDao.deleteCategory(category);
    }

    public void deleteCategoryById(Long id) {
        categoryDao.deleteCategoryById(id);
    }

    @Transactional(readOnly = true)
    public List<CategoryDTO> findAllCategories() {
        List<Category> categories = categoryDao.findAllCategories();
        return categoryMapper.toDTOs(categories);
    }

    public void deleteAllCategories() {
        categoryDao.deleteAllCategories();
    }
}
