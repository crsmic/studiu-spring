package com.open.quiz.service.mapper;

import com.open.quiz.model.QuizParticipant;
import com.open.quiz.service.dto.QuizParticipantDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {QuizMapper.class, ParticipantMapper.class, QuizParticipantAnswerMapper.class})
public interface QuizParticipantMapper extends EntityMapper<QuizParticipantDTO, QuizParticipant> {

    @Mapping(source = "quiz.id", target = "quizId")
    @Mapping(source = "participant.id", target = "participantId")
    QuizParticipantDTO toDTO(QuizParticipant quizParticipant);

    @Mapping(source = "quizId", target = "quiz")
    @Mapping(source = "participantId", target = "participant")
    QuizParticipant toEntity(QuizParticipantDTO quizParticipantDTO);

    default QuizParticipant fromId(Long id) {
        if (id == null) {
            return null;
        }
        QuizParticipant quizParticipant = new QuizParticipant();
        quizParticipant.setId(id);
        return quizParticipant;
    }
}
