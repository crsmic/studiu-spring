package com.open.quiz.service;

import com.open.quiz.dao.impl.CategoryTypeDao;
import com.open.quiz.model.CategoryType;
import com.open.quiz.service.dto.CategoryTypeDTO;
import com.open.quiz.service.mapper.CategoryTypeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CategoryTypeService {

    @Autowired
    private CategoryTypeDao categoryTypeDao;

    @Autowired
    private CategoryTypeMapper categoryTypeMapper;

    public CategoryTypeDTO createCategoryType(CategoryTypeDTO categoryTypeDTO){
        CategoryType categoryType = categoryTypeMapper.toEntity(categoryTypeDTO);
        categoryType = categoryTypeDao.createCategoryType(categoryType);
        return categoryTypeMapper.toDTO(categoryType);
    }

    public CategoryTypeDTO updateCategoryType(CategoryTypeDTO categoryTypeDTO){
        CategoryType categoryType = categoryTypeMapper.toEntity(categoryTypeDTO);
        categoryType = categoryTypeDao.updateCategoryType(categoryType);
        return categoryTypeMapper.toDTO(categoryType);
    }

    @Transactional(readOnly = true)
    public CategoryTypeDTO findCategoryTypeById(Long id) {
        CategoryType categoryType = categoryTypeDao.findCategoryTypeById(id);
        return categoryTypeMapper.toDTO(categoryType);
    }

    public void deleteCategoryType(CategoryTypeDTO categoryTypeDTO) {
        CategoryType categoryType = categoryTypeMapper.toEntity(categoryTypeDTO);
        categoryTypeDao.deleteCategoryType(categoryType);
    }

    public void deleteCategoryTypeById(Long id) {
        categoryTypeDao.deleteCategoryTypeById(id);
    }

    @Transactional(readOnly = true)
    public List<CategoryTypeDTO> findAllCategoryTypes() {
        List<CategoryType> categoryTypes = categoryTypeDao.findAllCategoryTypes();
        return categoryTypeMapper.toDTOs(categoryTypes);
    }

    public void deleteAllCategoryTypes() {
        categoryTypeDao.deleteAllCategoryTipes();
    }
}
