package com.open.quiz.service.mapper;

import com.open.quiz.model.Token;
import com.open.quiz.service.dto.TokenDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = {ParticipantMapper.class, QuizMapper.class})
public interface TokenMapper extends EntityMapper<TokenDTO, Token> {

    @Mapping(source = "participant.id", target = "participantId")
    @Mapping(source = "quiz.id", target = "quizId")
    TokenDTO toDTO(Token token);

    @Mapping(source = "participantId", target = "participant")
    @Mapping(source = "quizId", target = "quiz")
    Token toEntity(TokenDTO tokenDTO);

    List<Token> toEntities(List<TokenDTO> dtoList);

    List<TokenDTO> toDTOs(List<Token> entityList);

    default Token fromId(Long id) {
        if (id == null) {
            return null;
        }
        Token token = new Token();
        token.setId(id);
        return token;
    }

}
