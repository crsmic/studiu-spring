package com.open.quiz.service.mapper;

import com.open.quiz.model.Privilege;
import com.open.quiz.service.dto.PrivilegeDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = {RoleMapper.class})
public interface PrivilegeMapper extends EntityMapper<PrivilegeDTO, Privilege> {

    PrivilegeDTO toDTO(Privilege privilege);

    Privilege toEntity(PrivilegeDTO privilegeDTO);

    List<Privilege> toEntities(List<PrivilegeDTO> dtoList);

    List<PrivilegeDTO> toDTOs(List<Privilege> entityList);

    default Privilege fromId(Long id) {
        if (id == null) {
            return null;
        }
        Privilege privilege = new Privilege();
        privilege.setId(id);
        return privilege;
    }
    
}
