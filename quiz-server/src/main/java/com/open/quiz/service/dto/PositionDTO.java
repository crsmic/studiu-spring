package com.open.quiz.service.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class PositionDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(min = 1, max = 100)
    private String title;

    private Set<QuizDTO> quizDTOs = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Set<QuizDTO> getQuizDTOs() {
        return quizDTOs;
    }

    public void setQuizDTOs(Set<QuizDTO> quizDTOs) {
        this.quizDTOs = quizDTOs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PositionDTO)) return false;
        PositionDTO that = (PositionDTO) o;
        return Objects.equals(getId(), that.getId()) &&
            Objects.equals(getTitle(), that.getTitle()) &&
            Objects.equals(getQuizDTOs(), that.getQuizDTOs());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getTitle(), getQuizDTOs());
    }

    @Override
    public String toString() {
        return "PositionDTO{" +
            "id=" + id +
            ", title='" + title + '\'' +
            ", quizDTOs=" + quizDTOs +
            '}';
    }
}
