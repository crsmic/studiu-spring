package com.open.quiz.service.privilege;

import com.open.quiz.dao.impl.RoleDAO;
import com.open.quiz.dao.impl.UserDAO;
import com.open.quiz.model.Role;
import com.open.quiz.model.User;
import com.open.quiz.service.UserService;
import com.open.quiz.service.dto.RoleDTO;
import com.open.quiz.service.mapper.RoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

    @Service("userDetailsService")
    @Transactional
    public class MyUserDetailsService {

        @Autowired
        private UserDAO userRepository;

        @Autowired
        private UserService service;

        @Autowired
        private MessageSource messages;

        @Autowired
        private RoleDAO roleRepository;

//        @Override
//        public UserDetails loadUserByUsername(String email)
//                throws UsernameNotFoundException {
//
//            User user = userRepository.findByEmail(email);
//            if (user == null) {
//                return new org.springframework.security.core.userdetails.User(
//                        " ", " ", true, true, true, true,
//                        getAuthorities(Arrays.asList(
//                                roleRepository.findByName("ROLE_USER"))));
//            }
//
//            return new org.springframework.security.core.userdetails.User(
//                    user.getEmail(), user.getPassword(), user.isEnabled(), true, true,
//                    true, getAuthorities(user.getRoles()));
//        }
//
//        private Collection<? extends GrantedAuthority> getAuthorities(
//                Collection<Role> roles) {
//
//            return getGrantedAuthorities(getPrivileges(roles));
//        }
//
//        private List<String> getPrivileges(Collection<Role> roles) {
//
//            List<String> privileges = new ArrayList<>();
//            List<Privilege> collection = new ArrayList<>();
//            for (Role role : roles) {
//                collection.addAll(role.getPrivileges());
//            }
//            for (Privilege item : collection) {
//                privileges.add(item.getName());
//            }
//            return privileges;
//        }
//
//        private List<GrantedAuthority> getGrantedAuthorities(List<String> privileges) {
//            List<GrantedAuthority> authorities = new ArrayList<>();
//            for (String privilege : privileges) {
//                authorities.add(new SimpleGrantedAuthority(privilege));
//            }
//            return authorities;
//        }
    }