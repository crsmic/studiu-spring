package com.open.quiz.service;

import com.open.quiz.dao.impl.RoleDAO;
import com.open.quiz.model.Role;
import com.open.quiz.service.dto.RoleDTO;
import com.open.quiz.service.mapper.RoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class RoleService {

    @Autowired
    private RoleDAO roleDAO;

    @Autowired
    private RoleMapper roleMapper;

    public RoleDTO createRole(RoleDTO roleDTO) {
        Role role = roleMapper.toEntity(roleDTO);
        role = roleDAO.createRole(role);
        return roleMapper.toDTO(role);
    }

    public RoleDTO update(RoleDTO roleDTO) {
        Role role = roleMapper.toEntity(roleDTO);
        role = roleDAO.updateRole(role);
        return roleMapper.toDTO(role);
    }

    @Transactional(readOnly = true)
    public RoleDTO findRoleById(Long id) {
        Role role = roleDAO.findRoleById(id);
        return roleMapper.toDTO(role);
    }

    public void deleteRoleById(Long id) {
        roleDAO.deleteRoleById(id);
    }

    public void deleteAllRoles() {
        roleDAO.deleteAllRoles();
    }

    @Transactional(readOnly = true)
    public List<RoleDTO> findAllRoles() {
        List<Role> roles = roleDAO.findAllRoles();
        return roleMapper.toDTOs(roles);
    }
}
