package com.open.quiz.service.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class ParticipantDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(min = 3, max = 20)
    private String username;

    @NotNull
    @Size(min = 3, max = 15)
    private String password;

    @NotNull
    @Size(min = 3, max = 20)
    private String firstname;

    @NotNull
    @Size(min = 3, max = 20)
    private String lastname;

    @NotNull
    @Size(min = 3, max = 50)
    private String email;

    @NotNull
    @Size(min = 3, max = 12)
    private String phonenumber;

    private Set<QuizParticipantDTO> quizParticipantDTOs = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public Set<QuizParticipantDTO> getQuizParticipantDTOs() {
        return quizParticipantDTOs;
    }

    public void setQuizParticipantDTOs(Set<QuizParticipantDTO> quizParticipantDTOs) {
        this.quizParticipantDTOs = quizParticipantDTOs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ParticipantDTO)) return false;
        ParticipantDTO that = (ParticipantDTO) o;
        return Objects.equals(getId(), that.getId()) &&
            Objects.equals(getUsername(), that.getUsername()) &&
            Objects.equals(getPassword(), that.getPassword()) &&
            Objects.equals(getFirstname(), that.getFirstname()) &&
            Objects.equals(getLastname(), that.getLastname()) &&
            Objects.equals(getEmail(), that.getEmail()) &&
            Objects.equals(getPhonenumber(), that.getPhonenumber()) &&
            Objects.equals(getQuizParticipantDTOs(), that.getQuizParticipantDTOs());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getUsername(), getPassword(), getFirstname(), getLastname(), getEmail(), getPhonenumber(), getQuizParticipantDTOs());
    }

    @Override
    public String toString() {
        return "ParticipantDTO{" +
            "id=" + id +
            ", username='" + username + '\'' +
            ", password='" + password + '\'' +
            ", firstname='" + firstname + '\'' +
            ", lastname='" + lastname + '\'' +
            ", email='" + email + '\'' +
            ", phonenumber='" + phonenumber + '\'' +
            ", quizParticipantDTOs=" + quizParticipantDTOs +
            '}';
    }
}
