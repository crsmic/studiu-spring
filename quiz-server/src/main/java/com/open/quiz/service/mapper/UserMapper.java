package com.open.quiz.service.mapper;

import com.open.quiz.model.User;
import com.open.quiz.service.dto.UserDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = {RoleMapper.class})
public interface UserMapper extends EntityMapper<UserDTO, User> {

    UserDTO toDTO(User user);

    User toEntity(UserDTO userDTO);

    List<User> toEntities(List<UserDTO> dtoList);

    List<UserDTO> toDTOs(List<User> entityList);

    default User fromId(Long id) {
        if (id == null) {
            return null;
        }
        User user = new User();
        user.setId(id);
        return user;
    }
    
}
