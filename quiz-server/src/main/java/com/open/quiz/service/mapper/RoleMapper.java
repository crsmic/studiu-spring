package com.open.quiz.service.mapper;

import com.open.quiz.model.Role;
import com.open.quiz.service.dto.RoleDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = {ParticipantMapper.class, UserMapper.class, PrivilegeMapper.class})
public interface RoleMapper extends EntityMapper<RoleDTO, Role> {

    RoleDTO toDTO(Role role);

    Role toEntity(RoleDTO roleDTO);

    List<Role> toEntities(List<RoleDTO> dtoList);

    List<RoleDTO> toDTOs(List<Role> entityList);

    default Role fromId(Long id) {
        if (id == null) {
            return null;
        }
        Role role = new Role();
        role.setId(id);
        return role;
    }
}
