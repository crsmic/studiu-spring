package com.open.quiz.service;

import com.open.quiz.dao.impl.UserDAO;
import com.open.quiz.jwtsecurity.jwt.JwtTokenManager;
import com.open.quiz.model.User;
import com.open.quiz.service.dto.TokenDTO;
import com.open.quiz.service.dto.UserDTO;
import com.open.quiz.service.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserService {
    @Value("${app.jwtExpirationInSeconds}")
    private int jwtExpirationInSeconds;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenManager jwtTokenManager;


//    public Boolean existsByUsername(String username) {
//        return userDAO.existsByUsername(username);
//    }

    public UserDTO createUser(UserDTO userDTO) {
        User user = userMapper.toEntity(userDTO);
        String encryptedPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(encryptedPassword);
       // user.setUserRoles(userDTO.getRoles());
        user = userDAO.createUser(user);
        return userMapper.toDTO(user);
    }

    public void registerToken(UserDTO userDTO, Long userId){
    Authentication authentication = authenticationManager.authenticate(
            new UsernamePasswordAuthenticationToken(
                    userDTO.getUsername(),
                    userDTO.getPassword()
            )
    );

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtTokenManager.generateJwt(authentication);
        TokenDTO token = new TokenDTO();
        token.setParticipantId(userId);
        token.setToken(jwt);
        token.setTimeOfSolving(30);
        LocalDateTime expirationDate = LocalDateTime.now().plusSeconds(jwtExpirationInSeconds);
        token.setValability(expirationDate);
        tokenService.createToken(token);
    }

    @Transactional(readOnly = true)
    public UserDTO findUserById(Long id) {
        Optional<User> user = userDAO.findUserById(id);
        return userMapper.toDTO(user.orElse(null));
    }

   // @Transactional(readOnly = true)
    public UserDTO findUserByUsername(String username) {
        Optional<User> user = userDAO.findUserByUsername(username);
        return userMapper.toDTO(user.orElse(null));
    }

    @Transactional(readOnly = true)
    public UserDTO findUserByEmail(String email) {
        Optional<User> user = userDAO.findUserByEmail(email);
        return userMapper.toDTO(user.orElse(null));
    }

    @Transactional(readOnly = true)
    public List<UserDTO> findAllUsers() {
        List<User> users = userDAO.findAllUsers();
        return userMapper.toDTOs(users);
    }

    public UserDTO update(UserDTO userDTO) {
        User user = userMapper.toEntity(userDTO);
        user = userDAO.updateUser(user);
        return userMapper.toDTO(user);
    }

    public void deleteUserById(Long id) {
        userDAO.deleteUserById(id);
    }

    public void deleteAllUsers() {
        userDAO.deleteAllUsers();
    }

}
