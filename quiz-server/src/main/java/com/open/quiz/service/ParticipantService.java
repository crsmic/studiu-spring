package com.open.quiz.service;

import com.open.quiz.dao.impl.ParticipantDao;
import com.open.quiz.model.Participant;
import com.open.quiz.service.dto.ParticipantDTO;
import com.open.quiz.service.mapper.ParticipantMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ParticipantService {

    @Autowired
    private ParticipantDao participantDao;

    @Autowired
    private ParticipantMapper participantMapper;

    public ParticipantDTO createParticipant(ParticipantDTO participantDTO){
        Participant participant = participantMapper.toEntity(participantDTO);
        participant = participantDao.createParticipant(participant);
        return participantMapper.toDTO(participant);
    }

    public ParticipantDTO updateParticipant(ParticipantDTO participantDTO){
        Participant participant = participantMapper.toEntity(participantDTO);
        participant = participantDao.updateParticipant(participant);
        return participantMapper.toDTO(participant);
    }

    @Transactional(readOnly = true)
    public ParticipantDTO findParticipantById(Long id) {
        Participant participant = participantDao.findParticipantById(id);
        return participantMapper.toDTO(participant);
    }

    public void deleteParticipant(ParticipantDTO participantDTO) {
        Participant participant = participantMapper.toEntity(participantDTO);
        participantDao.deleteParticipant(participant);
    }

    public void deleteParticipantById(Long id) {
        participantDao.deleteParticipantById(id);
    }

    @Transactional(readOnly = true)
    public List<ParticipantDTO> findAllParticipants() {
        List<Participant> participants = participantDao.findAllParticipants();
        return participantMapper.toDTOs(participants);
    }

    public void deleteAllParticipants() {
        participantDao.deleteAllParticipants();
    }
}
