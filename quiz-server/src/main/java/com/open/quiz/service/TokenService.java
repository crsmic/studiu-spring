package com.open.quiz.service;

import com.open.quiz.dao.impl.TokenDAO;
import com.open.quiz.model.Token;
import com.open.quiz.service.dto.TokenDTO;
import com.open.quiz.service.mapper.TokenMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class TokenService {

    @Autowired
    private TokenDAO tokenDao;

    @Autowired
    private TokenMapper tokenMapper;

    public TokenDTO createToken(TokenDTO tokenDTO) {
        Token token = tokenMapper.toEntity(tokenDTO);
        token = tokenDao.createToken(token);
        return tokenMapper.toDTO(token);
    }

    @Transactional(readOnly = true)
    public TokenDTO findTokenById(Long id) {
        Token token = tokenDao.findTokenById(id);
        return tokenMapper.toDTO(token);
    }

    @Transactional(readOnly = true)
    public List<TokenDTO> findAllTokens() {
        List<Token> tokens = tokenDao.findAllTokens();
        return tokenMapper.toDTOs(tokens);
    }

    public TokenDTO update(TokenDTO tokenDTO) {
        Token token = tokenMapper.toEntity(tokenDTO);
        token = tokenDao.updateToken(token);
        return tokenMapper.toDTO(token);
    }

    public void deleteTokenById(Long id) {
        tokenDao.deleteTokenById(id);
    }

    public void deleteAllTokens() {
        tokenDao.deleteAllTokens();
    }

}
