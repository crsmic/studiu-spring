package com.open.quiz.service.mapper;

import com.open.quiz.model.Participant;
import com.open.quiz.service.dto.ParticipantDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {QuizParticipantMapper.class})
public interface ParticipantMapper extends EntityMapper<ParticipantDTO, Participant> {

    default Participant fromId(Long id) {
        if (id == null) {
            return null;
        }
        Participant participant = new Participant();
        participant.setId(id);
        return participant;
    }
}
