package com.open.quiz.service.mapper;

import com.open.quiz.model.QuizParticipantAnswer;
import com.open.quiz.service.dto.QuizParticipantAnswerDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {QuizParticipantMapper.class, QuestionMapper.class})
public interface QuizParticipantAnswerMapper extends EntityMapper<QuizParticipantAnswerDTO, QuizParticipantAnswer> {

    @Mapping(source = "quizParticipant.id", target = "quizParticipantId")
    @Mapping(source = "question.id", target = "questionId")
    QuizParticipantAnswerDTO toDTO(QuizParticipantAnswer quizParticipantAnswer);

    @Mapping(source = "quizParticipantId", target = "quizParticipant")
    @Mapping(source = "questionId", target = "question")
    QuizParticipantAnswer toEntity(QuizParticipantAnswerDTO quizParticipantAnswerDTO);

    default QuizParticipantAnswer fromId(Long id) {
        if (id == null) {
            return null;
        }
        QuizParticipantAnswer quizParticipantAnswer = new QuizParticipantAnswer();
        quizParticipantAnswer.setId(id);
        return quizParticipantAnswer;
    }
}
