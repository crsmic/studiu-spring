package com.open.quiz.service.dto;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class QuizDTO implements Serializable {

    private Long id;

    @Size(min = 3, max = 1000)
    private String quizDescription;

    private Long positionId;

    private Set<QuizParticipantDTO> quizParticipantDTOs = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuizDescription() {
        return quizDescription;
    }

    public void setQuizDescription(String quizDescription) {
        this.quizDescription = quizDescription;
    }

    public Long getPositionId() {
        return positionId;
    }

    public void setPositionId(Long positionId) {
        this.positionId = positionId;
    }

    public Set<QuizParticipantDTO> getQuizParticipantDTOs() {
        return quizParticipantDTOs;
    }

    public void setQuizParticipantDTOs(Set<QuizParticipantDTO> quizParticipantDTOs) {
        this.quizParticipantDTOs = quizParticipantDTOs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof QuizDTO)) return false;
        QuizDTO quizDTO = (QuizDTO) o;
        return Objects.equals(getId(), quizDTO.getId()) &&
            Objects.equals(getQuizDescription(), quizDTO.getQuizDescription()) &&
            Objects.equals(getPositionId(), quizDTO.getPositionId()) &&
            Objects.equals(getQuizParticipantDTOs(), quizDTO.getQuizParticipantDTOs());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getQuizDescription(), getPositionId(), getQuizParticipantDTOs());
    }

    @Override
    public String toString() {
        return "QuizDTO{" +
            "id=" + id +
            ", quizDescription='" + quizDescription + '\'' +
            ", positionId=" + positionId +
            ", quizParticipantDTOs=" + quizParticipantDTOs +
            '}';
    }
}
