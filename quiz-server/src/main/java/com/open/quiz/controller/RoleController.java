package com.open.quiz.controller;

import com.open.quiz.service.RoleService;
import com.open.quiz.service.dto.RoleDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping("/api")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @PostMapping("/roles")
    public ResponseEntity<RoleDTO> createRole(@Valid @RequestBody RoleDTO roleDTO) {
        RoleDTO result;
        HttpHeaders headers = new HttpHeaders();
        try {
            result = roleService.createRole(roleDTO);
            headers.setLocation(new URI("/api/roles" + result.getId()));
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, headers, CREATED);
    }

    @GetMapping("/roles/{id}")
    public ResponseEntity<RoleDTO> getRole(@PathVariable Long id) {
        RoleDTO result;
        try {
            result = roleService.findRoleById(id);
            if (result.getId() == null) {
                return new ResponseEntity<>(NO_CONTENT);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, OK);
    }

    @GetMapping("/roles")
    public ResponseEntity<List<RoleDTO>> getAllRoles() {
        List<RoleDTO> result;
        try {
            result = roleService.findAllRoles();
            if (result.isEmpty()) {
                return new ResponseEntity<>(NO_CONTENT);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, OK);
    }

    @PutMapping("/roles")
    public ResponseEntity<RoleDTO> updateRole(@Valid @RequestBody RoleDTO roleDTO) {
        RoleDTO result;
        if (roleDTO.getId() == null) {
            return createRole(roleDTO);
        }
        try {
            result = roleService.update(roleDTO);
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, OK);
    }

    @DeleteMapping("/roles/{id}")
    public ResponseEntity<HttpStatus> deleteRoleById(@PathVariable Long id) {
        try {
            roleService.deleteRoleById(id);
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(OK);
    }

    @DeleteMapping("/roles")
    public ResponseEntity<HttpStatus> deleteAllRoles() {
        try {
            roleService.deleteAllRoles();
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(OK);
    }
}
