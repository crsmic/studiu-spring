package com.open.quiz.controller;

import com.open.quiz.service.PositionService;
import com.open.quiz.service.dto.PositionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping("/api")
public class PositionController {

    @Autowired
    private PositionService positionService;

    @PostMapping("/positions")
    public ResponseEntity<PositionDTO> createPosition(@Valid @RequestBody PositionDTO positionDTO) {
        PositionDTO result;
        HttpHeaders headers = new HttpHeaders();
        try {
            result = positionService.createPosition(positionDTO);
            headers.setLocation(new URI("/api/positions/" + result.getId()));
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, headers, CREATED);
    }

    @PutMapping("/positions")
    public ResponseEntity<PositionDTO> updatePosition(@Valid @RequestBody PositionDTO positionDTO) {
        PositionDTO result;
        if (positionDTO.getId() == null) {
            return createPosition(positionDTO);
        }
        try {
            result = positionService.updatePosition(positionDTO);
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, OK);
    }

    @GetMapping("/positions/{id}")
    public ResponseEntity<PositionDTO> getPosition(@PathVariable Long id) {
        PositionDTO result;
        try {
            result = positionService.findPositionById(id);
            if (result.getId() == null) {
                return new ResponseEntity<>(NO_CONTENT);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, OK);
    }

    @DeleteMapping("/positions/{id}")
    public ResponseEntity<HttpStatus> deletePosition(@PathVariable Long id) {
        try {
            positionService.deletePositionById(id);
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(OK);
    }

    @GetMapping("/positions")
    public ResponseEntity<List<PositionDTO>> getAllPositions() {
        List<PositionDTO> result;
        try {
            result = positionService.findAllPositions();
            if (result.isEmpty()) {
                return new ResponseEntity<>(NO_CONTENT);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, OK);
    }

    @DeleteMapping("/positions")
    public ResponseEntity<HttpStatus> deleteAllPositions() {
        try {
            positionService.deleteAllPositions();
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(OK);
    }
}
