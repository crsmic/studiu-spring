package com.open.quiz.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;

@RestController
//@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/api")
public class TestRestAPIs {

    @GetMapping("/test/user")
    public String userAccess() {
        return ">>> User Contents!";
    }

    @GetMapping("/test/pm")
    public String projectManagementAccess() {
        return ">>> Board Management Project";
    }

//    @RolesAllowed("ADMIN")
//    @RolesAllowed({"ADMIN", "ROLE_ADMIN"})
    @GetMapping("/test/admin")
//    @PreAuthorize("hasRole('ADMIN') OR hasRole('ROLE_ADMIN')")
//    @PreAuthorize("hasRole('ADMIN')")
//    @Secured({"ROLE_ADMIN", "ADMIN"})
    @Secured("ROLE_ADMIN")
    public String adminAccess() {
        return ">>> Admin Contents";
    }
}
