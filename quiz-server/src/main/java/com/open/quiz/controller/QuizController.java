package com.open.quiz.controller;

import com.open.quiz.service.QuizService;
import com.open.quiz.service.dto.QuizDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping("/api")
public class QuizController {

    @Autowired
    private QuizService quizService;

    @PostMapping("/quizzes")
    public ResponseEntity<QuizDTO> createQuiz(@Valid @RequestBody QuizDTO quizDTO) {
        QuizDTO result;
        HttpHeaders headers = new HttpHeaders();
        try {
            result = quizService.createQuiz(quizDTO);
            headers.setLocation(new URI("/api/quiz/" + result.getId()));
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, headers, CREATED);
    }

    @PutMapping("/quizzes")
    public ResponseEntity<QuizDTO> updateQuiz(@Valid @RequestBody QuizDTO quizDTO) {
        QuizDTO result;
        if (quizDTO.getId() == null) {
            return createQuiz(quizDTO);
        }
        try {
            result = quizService.updateQuiz(quizDTO);
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, OK);
    }

    @GetMapping("/quizzes/{id}")
    public ResponseEntity<QuizDTO> getQuiz(@PathVariable Long id) {
        QuizDTO result;
        try {
            result = quizService.findQuizById(id);
            if (result.getId() == null) {
                return new ResponseEntity<>(NO_CONTENT);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, OK);
    }

    @DeleteMapping("/quizzes/{id}")
    public ResponseEntity<HttpStatus> deleteQuiz(@PathVariable Long id) {
        try {
            quizService.deleteQuizById(id);
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(OK);
    }

    @GetMapping("/quizzes")
    public ResponseEntity<List<QuizDTO>> getAllQuizs() {
        List<QuizDTO> result;
        try {
            result = quizService.findAllQuizs();
            if (result.isEmpty()) {
                return new ResponseEntity<>(NO_CONTENT);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, OK);
    }

    @DeleteMapping("/quizzes")
    public ResponseEntity<HttpStatus> deleteAllQuizs() {
        try {
            quizService.deleteAllQuizs();
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(OK);
    }

}
