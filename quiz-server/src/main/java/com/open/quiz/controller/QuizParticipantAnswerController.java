package com.open.quiz.controller;

import com.open.quiz.service.QuizParticipantAnswerService;
import com.open.quiz.service.dto.QuizParticipantAnswerDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping("/api")
public class QuizParticipantAnswerController {

    @Autowired
    private QuizParticipantAnswerService quizParticipantAnswerService;

    @PostMapping("/quizParticipantAnswers")
    public ResponseEntity<QuizParticipantAnswerDTO> createQuizParticipantAnswer(@Valid @RequestBody QuizParticipantAnswerDTO participantAnswerDTO) {
        QuizParticipantAnswerDTO result;
        HttpHeaders headers = new HttpHeaders();
        try {
            result = quizParticipantAnswerService.createQuizParticipantAnswer(participantAnswerDTO);
            headers.setLocation(new URI("/api/quizParticipantAnswers/" + result.getId()));
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, headers, CREATED);
    }

    @PutMapping("/quizParticipantAnswers")
    public ResponseEntity<QuizParticipantAnswerDTO> updateQuizParticipantAnswer(@Valid @RequestBody QuizParticipantAnswerDTO participantAnswerDTO) {
        QuizParticipantAnswerDTO result;
        if (participantAnswerDTO.getId() == null) {
            return createQuizParticipantAnswer(participantAnswerDTO);
        }
        try {
            result = quizParticipantAnswerService.updateQuizParticipantAnswer(participantAnswerDTO);
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, OK);
    }

    @GetMapping("/quizParticipantAnswers/{id}")
    public ResponseEntity<QuizParticipantAnswerDTO> getQuizParticipantAnswer(@PathVariable Long id) {
        QuizParticipantAnswerDTO result;
        try {
            result = quizParticipantAnswerService.findQuizParticipantAnswerById(id);
            if (result.getId() == null) {
                return new ResponseEntity<>(NO_CONTENT);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, OK);
    }

    @DeleteMapping("/quizParticipantAnswers/{id}")
    public ResponseEntity<HttpStatus> deleteQuizParticipantAnswer(@PathVariable Long id) {
        try {
            quizParticipantAnswerService.deleteQuizParticipantAnswerById(id);
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(OK);
    }

    @GetMapping("/quizParticipantAnswers")
    public ResponseEntity<List<QuizParticipantAnswerDTO>> getAllQuizParticipantAnswers() {
        List<QuizParticipantAnswerDTO> result;
        try {
            result = quizParticipantAnswerService.findAllQuizParticipantAnswers();
            if (result.isEmpty()) {
                return new ResponseEntity<>(NO_CONTENT);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, OK);
    }

    @DeleteMapping("/quizParticipantAnswers")
    public ResponseEntity<HttpStatus> deleteAllQuizParticipantAnswers() {
        try {
            quizParticipantAnswerService.deleteAllQuizParticipantAnswers();
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(OK);
    }


}
