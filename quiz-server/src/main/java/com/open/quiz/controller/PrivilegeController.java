package com.open.quiz.controller;

import com.open.quiz.service.PrivilegeService;
import com.open.quiz.service.dto.PrivilegeDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping("/api")
public class PrivilegeController {

    @Autowired
    private PrivilegeService privilegeService;

    @PostMapping("/privileges")
    public ResponseEntity<PrivilegeDTO> createPrivilege(@Valid @RequestBody PrivilegeDTO privilegeDTO) {
        PrivilegeDTO result;
        HttpHeaders headers = new HttpHeaders();
        try {
            result = privilegeService.createPrivilege(privilegeDTO);
            headers.setLocation(new URI("/api/privileges" + result.getId()));
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, headers, CREATED);
    }

    @GetMapping("/privileges/{id}")
    public ResponseEntity<PrivilegeDTO> getPrivilegeById(@PathVariable Long id) {
        PrivilegeDTO result;
        try {
            result = privilegeService.findPrivilegeById(id);
            if (result.getId() == null) {
                return new ResponseEntity<>(NO_CONTENT);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, OK);
    }

    @GetMapping("/privileges")
    public ResponseEntity<List<PrivilegeDTO>> getAllPrivileges() {
        List<PrivilegeDTO> result;
        try {
            result = privilegeService.findAllPrivileges();
            if (result.isEmpty()) {
                return new ResponseEntity<>(NO_CONTENT);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, OK);
    }

    @PutMapping("/privileges")
    public ResponseEntity<PrivilegeDTO> updatePrivilege(@Valid @RequestBody PrivilegeDTO privilegeDTO) {
        PrivilegeDTO result;
        if (privilegeDTO.getId() == null) {
            return createPrivilege(privilegeDTO);
        }
        try {
            result = privilegeService.update(privilegeDTO);
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, OK);
    }

    @DeleteMapping("/privileges/{id}")
    public ResponseEntity<HttpStatus> deletePrivilegeById(@PathVariable Long id) {
        try {
            privilegeService.deletePrivilegeById(id);
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(OK);
    }

    @DeleteMapping("/privileges")
    public ResponseEntity<HttpStatus> deleteAllPrivileges() {
        try {
            privilegeService.deleteAllPrivileges();
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(OK);
    }
}
