package com.open.quiz.controller;

import com.open.quiz.service.CategoryTypeService;
import com.open.quiz.service.dto.CategoryTypeDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping("/api")
public class CategoryTypeController {

    @Autowired
    private CategoryTypeService categoryTypeService;

    @PostMapping("/categoryTypes")
    public ResponseEntity<CategoryTypeDTO> createCategoryType(@Valid @RequestBody CategoryTypeDTO categoryTypeDTO) {
        CategoryTypeDTO result;
        HttpHeaders headers = new HttpHeaders();
        try {
            result = categoryTypeService.createCategoryType(categoryTypeDTO);
            headers.setLocation(new URI("/api/categoryType/" + result.getId()));
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, headers, CREATED);
    }

    @PutMapping("/categoryTypes")
    public ResponseEntity<CategoryTypeDTO> updateCategoryType(@Valid @RequestBody CategoryTypeDTO categoryTypeDTO) {
        CategoryTypeDTO result;
        if (categoryTypeDTO.getId() == null) {
            return createCategoryType(categoryTypeDTO);
        }
        try {
            result = categoryTypeService.updateCategoryType(categoryTypeDTO);
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, OK);
    }

    @GetMapping("/categoryTypes/{id}")
    public ResponseEntity<CategoryTypeDTO> getCategoryType(@PathVariable Long id) {
        CategoryTypeDTO result;
        try {
            result = categoryTypeService.findCategoryTypeById(id);
            if (result.getId() == null) {
                return new ResponseEntity<>(NO_CONTENT);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, OK);
    }

    @DeleteMapping("/categoryTypes/{id}")
    public ResponseEntity<HttpStatus> deleteCategoryType(@PathVariable Long id) {
        try {
            categoryTypeService.deleteCategoryTypeById(id);
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(OK);
    }

    @GetMapping("/categoryTypes")
    public ResponseEntity<List<CategoryTypeDTO>> getAllCategoryTypes() {
        List<CategoryTypeDTO> result;
        try {
            result = categoryTypeService.findAllCategoryTypes();
            if (result.isEmpty()) {
                return new ResponseEntity<>(NO_CONTENT);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, OK);
    }

    @DeleteMapping("/categoryTypes")
    public ResponseEntity<HttpStatus> deleteAllCategoryTypes() {
        try {
            categoryTypeService.deleteAllCategoryTypes();
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(OK);
    }
}
