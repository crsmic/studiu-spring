package com.open.quiz.controller;

import com.open.quiz.service.AnswerService;
import com.open.quiz.service.dto.AnswerDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping("/api")
public class AnswerController {

    @Autowired
    private AnswerService answerService;

    @PostMapping("/answers")
    public ResponseEntity<AnswerDTO> createAnswer(@Valid @RequestBody AnswerDTO answerDTO) {
        AnswerDTO result;
        HttpHeaders headers = new HttpHeaders();
        try {
            result = answerService.createAnswer(answerDTO);
            headers.setLocation(new URI("/api/answer/" + result.getId()));
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, headers, CREATED);
    }

    @PutMapping("/answers")
    public ResponseEntity<AnswerDTO> updateAnswer(@Valid @RequestBody AnswerDTO answerDTO) {
        AnswerDTO result;
        if (answerDTO.getId() == null) {
            return createAnswer(answerDTO);
        }
        try {
            result = answerService.updateAnswer(answerDTO);
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, OK);
    }

    @GetMapping("/answers/{id}")
    public ResponseEntity<AnswerDTO> getAnswer(@PathVariable Long id) {
        AnswerDTO result;
        try {
            result = answerService.findAnswerById(id);
            if (result.getId() == null) {
                return new ResponseEntity<>(NO_CONTENT);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, OK);
    }

    @DeleteMapping("/answers/{id}")
    public ResponseEntity<HttpStatus> deleteAnswer(@PathVariable Long id) {
        try {
            answerService.deleteAnswerById(id);
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(OK);
    }

    @GetMapping("/answers")
    public ResponseEntity<List<AnswerDTO>> getAllAnswers() {
        List<AnswerDTO> result;
        try {
            result = answerService.findAllAnswers();
            if (result.isEmpty()) {
                return new ResponseEntity<>(NO_CONTENT);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, OK);
    }

    @DeleteMapping("/answers")
    public ResponseEntity<HttpStatus> deleteAllAnswers() {
        try {
            answerService.deleteAllAnswers();
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(OK);
    }
}
