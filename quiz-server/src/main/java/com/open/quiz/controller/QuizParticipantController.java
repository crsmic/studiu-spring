package com.open.quiz.controller;

import com.open.quiz.service.QuizParticipantService;
import com.open.quiz.service.dto.QuizParticipantDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping("/api")
public class QuizParticipantController {

    @Autowired
    private QuizParticipantService quizParticipantService;

    @PostMapping("/quizParticipants")
    public ResponseEntity<QuizParticipantDTO> createQuizParticipant(@Valid @RequestBody QuizParticipantDTO quizParticipantDTO) {
        QuizParticipantDTO result;
        HttpHeaders headers = new HttpHeaders();
        try {
            result = quizParticipantService.createQuizParticipant(quizParticipantDTO);
            headers.setLocation(new URI("/api/quizParticipants/" + result.getId()));
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, headers, CREATED);
    }

    @PutMapping("/quizParticipants")
    public ResponseEntity<QuizParticipantDTO> updateQuizParticipant(@Valid @RequestBody QuizParticipantDTO quizParticipantDTO) {
        QuizParticipantDTO result;
        if (quizParticipantDTO.getId() == null) {
            return createQuizParticipant(quizParticipantDTO);
        }
        try {
            result = quizParticipantService.updateQuizParticipant(quizParticipantDTO);
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, OK);
    }

    @GetMapping("/quizParticipants/{id}")
    public ResponseEntity<QuizParticipantDTO> getQuizParticipantById(@PathVariable Long id) {
        QuizParticipantDTO result;
        try {
            result = quizParticipantService.findQuizParticipantById(id);
            if (result.getId() == null) {
                return new ResponseEntity<>(NO_CONTENT);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, OK);
    }

    @DeleteMapping("/quizParticipants/{id}")
    public ResponseEntity<HttpStatus> deleteQuizParticipantById(@PathVariable Long id) {
        try {
            quizParticipantService.deleteQuizParticipantById(id);
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(OK);
    }

    @GetMapping("/quizParticipants")
    public ResponseEntity<List<QuizParticipantDTO>> getAllQuizParticipants() {
        List<QuizParticipantDTO> result;
        try {
            result = quizParticipantService.findAllQuizParticipants();
            if (result.isEmpty()) {
                return new ResponseEntity<>(NO_CONTENT);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, OK);
    }

    @DeleteMapping("/quizParticipants")
    public ResponseEntity<HttpStatus> deleteAllQuizParticipants() {
        try {
            quizParticipantService.deleteAllQuizParticipantss();
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(OK);
    }


}
