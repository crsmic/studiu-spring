package com.open.quiz.controller;

import com.open.quiz.dao.impl.UserDAO;
import com.open.quiz.jwtsecurity.jwt.JwtTokenManager;
import com.open.quiz.jwtsecurity.payload.ApiResponse;
import com.open.quiz.jwtsecurity.payload.AuthenticationResponse;
import com.open.quiz.jwtsecurity.payload.LoginRequest;
import com.open.quiz.mail.JavaMailSenderService;
import com.open.quiz.model.User;
import com.open.quiz.service.UserService;
import com.open.quiz.service.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserDAO userRepository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenManager jwtTokenManager;

    @Autowired
    private JavaMailSenderService mailSenderService;

    @PostMapping("/users/email")
    public ResponseEntity<ApiResponse> sendEmail() {
        mailSenderService.sendEmail("crsmic7@gmail.com");
        return ResponseEntity
                .ok()
                .body(new ApiResponse(true, "User registered successfully"));
    }

    @PostMapping("/users/signup")
    public ResponseEntity<ApiResponse> createUser(@Valid @RequestBody UserDTO userDTO) {
        if (userRepository.existsByUsername(userDTO.getUsername())) {
            return new ResponseEntity(new ApiResponse(false, "Username is already taken!"),
                    HttpStatus.BAD_REQUEST);
        }
        if (userRepository.existsByEmail(userDTO.getEmail())) {
            return new ResponseEntity(new ApiResponse(false, "Email Address already in use!"),
                    HttpStatus.BAD_REQUEST);
        }
        UserDTO userDTOcreated = userService.createUser(userDTO);
        userService.registerToken(userDTO, userDTOcreated.getId());
        return ResponseEntity
                .ok()
                .body(new ApiResponse(true, "User registered successfully"));
    }

    @PostMapping("/users/login")
    public ResponseEntity<AuthenticationResponse> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        String username = loginRequest.getUsername();
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsername(),
                        loginRequest.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtTokenManager.generateJwt(authentication);
        // 1. preluat token din header request
        // 2. verificat existenta in baza de date (per quizId??? + participantId) + validitate
        // 3. confrunt token din baza cu cel primit pe request
        // 4. dau raspuns boolean : OK / NOK
        // 5. getUsernameFromJwtToken - exista user in db ??
        AuthenticationResponse authenticationResponse = new AuthenticationResponse(jwt);
        authenticationResponse.setAuthUsername(username);
        User user = userRepository.findUserByUsername(username).orElse(null);
        Set<String> roles = user.getUserRoles().stream().map(role -> role.getRole()).collect(Collectors.toSet());
//        Set<String> roles = userService.findUserByUsername(username).getRoles().stream().map(role -> role.getRole()).collect(Collectors.toSet());
        authenticationResponse.setAuthAuthorities(roles);

        return Objects.requireNonNull(ResponseEntity
                .ok()
                .headers(new HttpHeaders())
                .contentType(MediaType.APPLICATION_JSON)
                .body(authenticationResponse));
    }

    @PostMapping("/logout")
    public ResponseEntity<ApiResponse> logoutUser(HttpServletRequest httpServletRequest) {
        httpServletRequest.getSession().invalidate();
        return ResponseEntity.status(HttpStatus.OK).body(new ApiResponse(true, "User signed off successfully! " + HttpStatus.OK));
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<UserDTO> getUserById(@PathVariable Long id) {
        UserDTO result;
        try {
            result = userService.findUserById(id);
            if (result.getId() == null) {
                return new ResponseEntity<>(NO_CONTENT);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, OK);
    }

    @GetMapping("/users")
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<List<UserDTO>> getAllUsers() {
        List<UserDTO> result;
        try {
            result = userService.findAllUsers();
            if (result.isEmpty()) {
                return new ResponseEntity<>(NO_CONTENT);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, OK);
    }

    @PutMapping("/users")
    public ResponseEntity<ApiResponse> updateUser(@Valid @RequestBody UserDTO userDTO) {
        if (userDTO.getId() == null) {
            createUser(userDTO);
            return ResponseEntity.status(HttpStatus.OK).body(new ApiResponse(true, "User created successfully! " + HttpStatus.OK));
        }
        userService.update(userDTO);
        return ResponseEntity.status(HttpStatus.OK).body(new ApiResponse(true, "User updated successfully! " + HttpStatus.OK));
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<HttpStatus> deleteUserById(@PathVariable Long id) {
        try {
            userService.deleteUserById(id);
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(OK);
    }

    @DeleteMapping("/users")
    public ResponseEntity<HttpStatus> deleteAllUsers() {
        try {
            userService.deleteAllUsers();
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(OK);
    }
}
