package com.open.quiz.controller;

import com.open.quiz.service.TokenService;
import com.open.quiz.service.dto.TokenDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping("/api")
public class TokenController {

    @Autowired
    private TokenService tokenService;

    @PostMapping("/tokens")
    public ResponseEntity<TokenDTO> createToken(@Valid @RequestBody TokenDTO tokenDTO) {
        TokenDTO result;
        HttpHeaders headers = new HttpHeaders();
        try {
            result = tokenService.createToken(tokenDTO);
            headers.setLocation(new URI("/api/token/" + result.getId()));
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, headers, CREATED);
    }

    @GetMapping("/tokens/{id}")
    public ResponseEntity<TokenDTO> getTokenById(@PathVariable Long id) {
        TokenDTO result;
        try {
            result = tokenService.findTokenById(id);
            if (result.getId() == null) {
                return new ResponseEntity<>(NO_CONTENT);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, OK);
    }

    @GetMapping("/tokens")
    public ResponseEntity<List<TokenDTO>> getAllTokens() {
        List<TokenDTO> result;
        try {
            result = tokenService.findAllTokens();
            if (result.isEmpty()) {
                return new ResponseEntity<>(NO_CONTENT);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, OK);
    }

    @PutMapping("/tokens")
    public ResponseEntity<TokenDTO> updateToken(@Valid @RequestBody TokenDTO tokenDTO) {
        TokenDTO result;
        if (tokenDTO.getId() == null) {
            return createToken(tokenDTO);
        }
        try {
            result = tokenService.update(tokenDTO);
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, OK);
    }

    @DeleteMapping("/tokens/{id}")
    public ResponseEntity<HttpStatus> deleteTokenById(@PathVariable Long id) {
        try {
            tokenService.deleteTokenById(id);
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(OK);
    }

    @DeleteMapping("/tokens")
    public ResponseEntity<HttpStatus> deleteAllTokens() {
        try {
            tokenService.deleteAllTokens();
        } catch (Exception e) {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(OK);
    }
}
