package com.open.quiz.model;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "token")
public class Token implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "token_sequenceGenerator")
    @SequenceGenerator(name = "token_sequenceGenerator", sequenceName = "token_id_seq", allocationSize = 1)
    private Long id;

    @Column(name = "token_text")
    private String token;

    @Column(name = "time")
    private int timeOfSolving;

    @Column(name = "valability")
    private LocalDateTime valability;

    @ManyToOne
    @JoinColumn(name = "participant_id")
    private Participant participant;

    @ManyToOne
    @JoinColumn(name = "quiz_id")
    private Quiz quiz;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getTimeOfSolving() {
        return timeOfSolving;
    }

    public void setTimeOfSolving(int timeOfSolving) {
        this.timeOfSolving = timeOfSolving;
    }

    public LocalDateTime getValability() {
        return valability;
    }

    public void setValability(LocalDateTime valability) {
        this.valability = valability;
    }

    public Participant getParticipant() {
        return participant;
    }

    public void setParticipant(Participant participant) {
        this.participant = participant;
    }

    public Quiz getQuiz() {
        return quiz;
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Token token1 = (Token) o;
        return timeOfSolving == token1.timeOfSolving &&
                id.equals(token1.id) &&
                token.equals(token1.token) &&
                valability.equals(token1.valability);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, token, timeOfSolving, valability);
    }

    @Override
    public String toString() {
        return "Token{" +
                "id=" + id +
                ", token='" + token + '\'' +
                ", timeOfSolving=" + timeOfSolving +
                ", valability=" + valability +
                '}';
    }
}
