package com.open.quiz.model;

import com.open.quiz.model.enumeration.Difficulty;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "question")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator", sequenceName = "question_id_seq", allocationSize = 1)
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "question_difficulty")
    private Difficulty questionDifficulty;

    @NotNull
    @Size(min = 3, max = 1000)
    @Column(name = "question_text", length = 1000)
    private String questionText;

    @NotNull
    @Column(name = "question_multi_answer")
    private Long questionMultiAnswer;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "category_id")
    private Category category;

    @OneToMany(mappedBy = "question")
    private Set<Answer> answers = new HashSet<>();

    @OneToMany(mappedBy = "question")
    private Set<QuizParticipantAnswer> quizParticipantAnswers = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Difficulty getQuestionDifficulty() {
        return questionDifficulty;
    }

    public void setQuestionDifficulty(Difficulty questionDifficulty) {
        this.questionDifficulty = questionDifficulty;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public Long getQuestionMultiAnswer() {
        return questionMultiAnswer;
    }

    public void setQuestionMultiAnswer(Long questionMultiAnswer) {
        this.questionMultiAnswer = questionMultiAnswer;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Set<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(Set<Answer> answers) {
        this.answers = answers;
    }

    public Set<QuizParticipantAnswer> getQuizParticipantAnswers() {
        return quizParticipantAnswers;
    }

    public void setQuizParticipantAnswers(Set<QuizParticipantAnswer> quizParticipantAnswers) {
        this.quizParticipantAnswers = quizParticipantAnswers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Question)) return false;
        Question question = (Question) o;
        return Objects.equals(getId(), question.getId()) &&
            getQuestionDifficulty() == question.getQuestionDifficulty() &&
            Objects.equals(getQuestionText(), question.getQuestionText()) &&
            Objects.equals(getQuestionMultiAnswer(), question.getQuestionMultiAnswer()) &&
            Objects.equals(getCategory(), question.getCategory()) &&
            Objects.equals(getAnswers(), question.getAnswers()) &&
            Objects.equals(getQuizParticipantAnswers(), question.getQuizParticipantAnswers());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getQuestionDifficulty(), getQuestionText(), getQuestionMultiAnswer(), getCategory(), getAnswers(), getQuizParticipantAnswers());
    }

    @Override
    public String toString() {
        return "Question{" +
            "id=" + id +
            ", questionDifficulty=" + questionDifficulty +
            ", questionText='" + questionText + '\'' +
            ", questionMultiAnswer=" + questionMultiAnswer +
            ", category=" + category +
            ", answers=" + answers +
            ", quizParticipantAnswers=" + quizParticipantAnswers +
            '}';
    }
}
