package com.open.quiz.model;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "quiz")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Quiz {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator", sequenceName = "quiz_id_seq", allocationSize = 1)
    private Long id;

    @Size(min = 3, max = 1000)
    @Column(name = "quiz_description", length = 1000)
    private String quizDescription;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "position_id")
    private Position position;

    @OneToMany(mappedBy = "quiz")
    private Set<QuizParticipant> quizParticipants = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuizDescription() {
        return quizDescription;
    }

    public void setQuizDescription(String quizDescription) {
        this.quizDescription = quizDescription;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Set<QuizParticipant> getQuizParticipants() {
        return quizParticipants;
    }

    public void setQuizParticipants(Set<QuizParticipant> quizParticipants) {
        this.quizParticipants = quizParticipants;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Quiz)) return false;
        Quiz quiz = (Quiz) o;
        return Objects.equals(getId(), quiz.getId()) &&
            Objects.equals(getQuizDescription(), quiz.getQuizDescription());

    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getQuizDescription());
    }

    @Override
    public String toString() {
        return "Quiz{" +
            "id=" + id +
            ", quizDescription='" + quizDescription + '\'' +
            ", position=" + position +
            '}';
    }
}
