package com.open.quiz.model;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "quiz_participant_answer")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class QuizParticipantAnswer {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator", sequenceName = "quiz_participant_answer_id_seq", allocationSize = 1)
    private Long id;

    @NotNull
    @Column(name = "given_answer_id")
    private Long givenAnswerId;

    @Column(name = "start_date")
    private LocalDateTime startDate;

    @Column(name = "end_date")
    private LocalDateTime endDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "quiz_participant_id")
    private QuizParticipant quizParticipant;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "question_id")
    private Question question;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGivenAnswerId() {
        return givenAnswerId;
    }

    public void setGivenAnswerId(Long givenAnswerId) {
        this.givenAnswerId = givenAnswerId;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public QuizParticipant getQuizParticipant() {
        return quizParticipant;
    }

    public void setQuizParticipant(QuizParticipant quizParticipant) {
        this.quizParticipant = quizParticipant;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof QuizParticipantAnswer)) return false;
        QuizParticipantAnswer that = (QuizParticipantAnswer) o;
        return Objects.equals(getId(), that.getId()) &&
            Objects.equals(getGivenAnswerId(), that.getGivenAnswerId()) &&
            Objects.equals(getStartDate(), that.getStartDate()) &&
            Objects.equals(getEndDate(), that.getEndDate()) &&
            Objects.equals(getQuizParticipant(), that.getQuizParticipant()) &&
            Objects.equals(getQuestion(), that.getQuestion());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getGivenAnswerId(), getStartDate(), getEndDate(), getQuizParticipant(), getQuestion());
    }

    @Override
    public String toString() {
        return "QuizParticipantAnswer{" +
            "id=" + id +
            ", givenAnswerId=" + givenAnswerId +
            ", startDate=" + startDate +
            ", endDate=" + endDate +
            ", quizParticipant=" + quizParticipant +
            ", question=" + question +
            '}';
    }
}
