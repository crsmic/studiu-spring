package com.open.quiz.model;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "participant")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Participant implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator", sequenceName = "participant_id_seq", allocationSize = 1)
    private Long id;

    @Size(min = 3, max = 20)
    @Column(name = "username", length = 20)
    private String username;

    @Size(min = 3, max = 15)
    @Column(name = "password", length = 15)
    private String password;

    @Size(min = 3, max = 20)
    @Column(name = "firstname", length = 20)
    private String firstname;

    @Size(min = 3, max = 20)
    @Column(name = "lastname", length = 20)
    private String lastname;

    @Size(min = 3, max = 50)
    @Column(name = "email", length = 50)
    private String email;

    @Size(min = 3, max = 12)
    @Column(name = "phonenumber", length = 12)
    private String phonenumber;

    @OneToMany(mappedBy = "participant", cascade = CascadeType.ALL)
    private Set<QuizParticipant> quizParticipants;

    @ManyToOne
    @JoinColumn(name = "role_id")
    private Role role;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public Participant username(String username) {
        this.username = username;
        return this;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public Participant password(String password) {
        this.password = password;
        return this;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstname() {
        return firstname;
    }

    public Participant firstname(String firstname) {
        this.firstname = firstname;
        return this;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public Participant lastname(String lastname) {
        this.lastname = lastname;
        return this;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public Participant email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public Participant phonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
        return this;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public Set<QuizParticipant> getQuizParticipants() {
        return quizParticipants;
    }

    public Participant quizParticipants(Set<QuizParticipant> quizParticipants) {
        this.quizParticipants = quizParticipants;
        return this;
    }

    public Participant addQuizParticipant(QuizParticipant quizParticipant) {
        this.quizParticipants.add(quizParticipant);
        quizParticipant.setParticipant(this);
        return this;
    }

    public Participant removeQuizParticipant(QuizParticipant quizParticipant) {
        this.quizParticipants.remove(quizParticipant);
        quizParticipant.setParticipant(null);
        return this;
    }

    public void setQuizParticipants(Set<QuizParticipant> quizParticipants) {
        this.quizParticipants = quizParticipants;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Participant)) return false;
        Participant that = (Participant) o;
        return getId().equals(that.getId()) &&
                getUsername().equals(that.getUsername()) &&
                getPassword().equals(that.getPassword()) &&
                getFirstname().equals(that.getFirstname()) &&
                getLastname().equals(that.getLastname()) &&
                getEmail().equals(that.getEmail()) &&
                getPhonenumber().equals(that.getPhonenumber());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getUsername(), getPassword(), getFirstname(), getLastname(), getEmail(), getPhonenumber());
    }

    @Override
    public String toString() {
        return "Participant{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", email='" + email + '\'' +
                ", phonenumber='" + phonenumber + '\'' +
                '}';
    }
}
