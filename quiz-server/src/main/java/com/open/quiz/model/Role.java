package com.open.quiz.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "role")
public class Role implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "role_sequenceGenerator")
    @SequenceGenerator(name = "role_sequenceGenerator", sequenceName = "role_id_seq", allocationSize = 1)
    private Long id;

    @Column(name = "role")
    private String role;

    @ManyToMany(mappedBy = "userRoles", fetch = FetchType.LAZY)
    private Set<User> users;

    @OneToMany(mappedBy = "role", fetch = FetchType.LAZY)
    private Set<Participant> participants;

    @ManyToMany
    @JoinTable(
            name = "role_privilege",
            joinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "privilege_id", referencedColumnName = "id"))
    private Set<Privilege> rolePrivileges;

    public Role(){

    }

    public Role(Long id, String role){
        this.id = id;
        this.role = role;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public Set<Participant> getParticipants() {
        return participants;
    }

    public void setParticipants(Set<Participant> participants) {
        this.participants = participants;
    }

    public Set<Privilege> getRolePrivileges() {
        return rolePrivileges;
    }

    public void setRolePrivileges(Set<Privilege> rolePrivileges) {
        this.rolePrivileges = rolePrivileges;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Role role1 = (Role) o;
        return id.equals(role1.id) &&
                role.equals(role1.role);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, role);
    }

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", role='" + role + '\'' +
                '}';
    }
}
