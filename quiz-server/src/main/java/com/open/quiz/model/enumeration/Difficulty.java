package com.open.quiz.model.enumeration;

public enum Difficulty {
    INTERN, JUNIOR, MIDDLE, SENIOR, EXPERT
}
