package com.open.quiz.model;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "answer")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Answer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator", sequenceName = "answer_id_seq", allocationSize = 1)
    private Long id;

    @NotNull
    @Size(min = 1, max = 1000)
    @Column(name = "answer_text", length = 1000)
    private String answerText;

    @NotNull
    @Column(name = "answer_correctness")
    private Boolean answerCorrectness;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "question_id")
    private Question question;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAnswerText() {
        return answerText;
    }

    public void setAnswerText(String answerText) {
        this.answerText = answerText;
    }

    public Boolean getAnswerCorrectness() {
        return answerCorrectness;
    }

    public void setAnswerCorrectness(Boolean answerCorrectness) {
        this.answerCorrectness = answerCorrectness;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Answer)) return false;
        Answer answer = (Answer) o;
        return Objects.equals(getId(), answer.getId()) &&
            Objects.equals(getAnswerText(), answer.getAnswerText()) &&
            Objects.equals(getAnswerCorrectness(), answer.getAnswerCorrectness()) &&
            Objects.equals(getQuestion(), answer.getQuestion());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getAnswerText(), getAnswerCorrectness(), getQuestion());
    }

    @Override
    public String toString() {
        return "Answer{" +
            "id=" + id +
            ", answerText='" + answerText + '\'' +
            ", answerCorrectness=" + answerCorrectness +
            ", question=" + question +
            '}';
    }
}
