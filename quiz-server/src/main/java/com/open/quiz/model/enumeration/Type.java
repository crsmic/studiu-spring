package com.open.quiz.model.enumeration;

public enum Type {
    JAVA, SPRING, HIBERNATE, SQL, LINUX
}
