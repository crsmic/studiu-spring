package com.open.quiz.mail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.Date;

@Service
public class JavaMailSenderService {

//    @Value("${spring.mail.host}")
//    private String host;
//    @Value("${spring.mail.port}")
//    private String port;
//    @Value("${spring.mail.username}")
//    private String username;
//    @Value("${spring.mail.password}")
//    private String password;

//    @Value("${spring.mail.properties.mail.smtp.auth}")
//    private String auth;
//    @Value("${spring.mail.properties.mail.smtp.connectiontimeout}")
//    private String connectiontimeout;
//    @Value("${spring.mail.properties.mail.smtp.timeout}")
//    private String timeout;
//    @Value("${spring.mail.properties.mail.smtp.writetimeout}")
//    private String writetimeout;

    @Autowired
    private JavaMailSender javaMailSender;

    public void sendEmail(String senders) {
        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setFrom("crsmic7@gmail.com");
        msg.setTo(senders);
        msg.setSubject("Testing from Spring Boot");
        msg.setText("Hello World \n Spring Boot Email");
        msg.setSentDate(new Date());

        javaMailSender.send(msg);
    }

    public void sendEmailWithAttachment() throws MessagingException, IOException {

        MimeMessage msg = javaMailSender.createMimeMessage();

        // true = multipart message
        MimeMessageHelper helper = new MimeMessageHelper(msg, true);
        helper.setTo("1@gmail.com");

        helper.setSubject("Testing from Spring Boot");

        // default = text/plain
        //helper.setText("Check attachment for image!");

        // true = text/html
        helper.setText("<h1>Check attachment for image!</h1>", true);

        helper.addAttachment("my_photo.png", new ClassPathResource("android.png"));

        javaMailSender.send(msg);

    }

}