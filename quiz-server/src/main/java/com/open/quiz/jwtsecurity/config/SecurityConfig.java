package com.open.quiz.jwtsecurity.config;

import com.open.quiz.jwtsecurity.jwt.JwtAuthenticationEntryPoint;
import com.open.quiz.jwtsecurity.jwt.JwtAuthenticationTokenFilter;
import com.open.quiz.jwtsecurity.security.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.util.Collections;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        securedEnabled = true, // @Secured({"ROLE_USER", "ROLE_ADMIN"}) or @Secured("IS_AUTHENTICATED_ANONYMOUSLY")
        jsr250Enabled = true, // enables the @RolesAllowed("ROLE_ADMIN") annotation
        prePostEnabled = true // enables the @PreAuthorize("isAnonymous()") annotation
)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsServiceImpl userDetailsServiceImpl;

    @Autowired
    private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

    @Autowired
    private AuthenticationProvider authenticationProvider;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManager() {
        return new ProviderManager(Collections.singletonList(authenticationProvider));
    }

    @Bean
    public JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter() {
        return new JwtAuthenticationTokenFilter();
    }

    @Bean
    public AuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setUserDetailsService(userDetailsServiceImpl);
        provider.setPasswordEncoder(new BCryptPasswordEncoder());
        return provider;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsServiceImpl).passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors().and().csrf().disable()
                .authorizeRequests()
                    .antMatchers("/",
                            "/favicon.ico",
                            "/**/*.png",
                            "/**/*.gif",
                            "/**/*.svg",
                            "/**/*.jpg",
                            "/**/*.html",
                            "/**/*.css",
                            "/**/*.js",
                            "/api/users/signup",
                            "/api/users/login",
                            "/api/users/logout",
                            "/api/test/user",
                            "/api/users/email",
                            "/api/test/**"
                    ).permitAll()

//                    .antMatchers("/api/test/admin")
////                        .access("hasRole('ADMIN')")
//                            .access("hasAuthority('ADMIN')")
////                        .hasAuthority("ADMIN")
////                            .hasRole("ADMIN")
//                    .anyRequest().authenticated()
                    .and()

//                .formLogin()
//                    .loginPage("/api/auth/login")
//                    .permitAll()
//                    .and()
                .logout()
                    .logoutUrl("/api/users/logout")
                    .invalidateHttpSession(true)
                    .clearAuthentication(true)
//                    .logoutRequestMatcher(new AntPathRequestMatcher("/api/users/logout"))
//                    .addLogoutHandler(logoutHandler)                                         6
//                    .deleteCookies(cookieNamesToClear)
                .and()
                .exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint)
                    .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                    .and()
                .httpBasic();
        http.addFilterBefore(jwtAuthenticationTokenFilter(), UsernamePasswordAuthenticationFilter.class);
    }

}
