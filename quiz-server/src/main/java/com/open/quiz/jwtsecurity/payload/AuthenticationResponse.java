package com.open.quiz.jwtsecurity.payload;

import java.util.Set;

public class AuthenticationResponse {

    private String authToken;
    private String tokenType = "Bearer";
    private String authUsername;
    private Set<String> authAuthorities;

    public String getAuthUsername() {
        return authUsername;
    }

    public void setAuthUsername(String authUsername) {
        this.authUsername = authUsername;
    }

    public Set<String> getAuthAuthorities() {
        return authAuthorities;
    }

    public void setAuthAuthorities(Set<String> authAuthorities) {
        this.authAuthorities = authAuthorities;
    }

    public AuthenticationResponse(String accessToken) {
        this.authToken = accessToken;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }


}
